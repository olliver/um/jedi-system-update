#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

usage()
{
    echo "Usage: ${0} [OPTIONS] <toolbox image file>"
    echo "Run this repositories tests, optionally using the <toolbox image file> as toolbox."
    echo "Without the optional parameter <toolbox image file>, legacy tests are skipped."
    echo "    -h   Print usage"
}

run_tests()
{
    for test_script in "./${1:-test}/"*".sh"; do
        if [ ! -x "${test_script}" ]; then
            echo "--------------------------------------------------------------------------------"
            echo "Warning, skipping test '${test_script}'."
            echo "--------------------------------------------------------------------------------"
            continue
        fi

        eval "${test_script}" "${2:-}"
    done
}

while getopts ":h" options; do
    case "${options}" in
    h)
        usage
        exit 0
        ;;
    :)
        echo "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo "Invalid option: -${OPTARG}"
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"

if [ "${#}" -ge 1 ]; then
    if [ ! -f "${1}" ]; then
        echo "Invalid parameter for <toolbox image file>."
        usage
        exit 1
    fi

    run_tests "legacy_test" "${1}"
fi

run_tests

exit 0
