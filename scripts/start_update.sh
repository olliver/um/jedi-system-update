#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# Common directory variables
PREFIX="/usr"
DATAROOTDIR="${PREFIX}/share"
DATADIR="${DATAROOTDIR}"
EXEC_PREFIX="${PREFIX}"
SYSCONFDIR="${SYSCONFDIR:-/etc}"
SBINDIR="${EXEC_PREFIX}/sbin"

CMDS=" \
    [ \
    echo \
    exit \
    mktemp \
    mount \
    mountpoint \
    rmdir \
    shift \
    umount \
"

SYSTEM_UPDATE_ENTRYPOINT="${SBINDIR}/start_update.sh"
TOOLBOX_IMAGE="${TOOLBOX_IMAGE:-${DATADIR}/jedi-system-update/um-update_toolbox.xz.img}"
TOOLBOX_MNT_TEMPLATE="um-toolbox"
UPDATE_ROOT_MNT_TEMPLATE="um-update_root_mnt"

usage()
{
    echo "Usage: ${0} [OPTIONS] <UPDATE_IMAGE> <TARGET_STORAGE_DEVICE>"
    echo "This is the update entry point script, it is responsible for setting up the"
    echo "environment in which the update toolbox can be used to configure and update"
    echo "the firmware."
    echo "  -h Print this help text and exit"
    echo ""
    echo "  [TARGET_STORAGE_DEVICE], device onto which to install the update"
    echo "  [UPDATE_IMAGE], the path to the update image file"
}

prepare()
{
    echo "Preparing toolbox ..."

    UPDATE_ROOT_MNT="$(mktemp -t -d "${UPDATE_ROOT_MNT_TEMPLATE}".XXXXXX)"

    if ! mount "${UPDATE_IMAGE}" "${UPDATE_ROOT_MNT}"; then
        echo "Error: failed to mount '${UPDATE_IMAGE}' to '${UPDATE_ROOT_MNT}'".
        exit 1
    fi

    UPDATE_FILE_SRC_DIR="$(dirname "${UPDATE_IMAGE}")"
    TOOLBOX_FILENAME="$(basename "${TOOLBOX_IMAGE}")"

    if ! cp -f "${UPDATE_ROOT_MNT}/${TOOLBOX_IMAGE}" "${UPDATE_FILE_SRC_DIR}/${TOOLBOX_FILENAME}"; then
        echo "Error: failed to copy '${UPDATE_ROOT_MNT}/${TOOLBOX_IMAGE}' to '${UPDATE_FILE_SRC_DIR}/${TOOLBOX_FILENAME}'".
        exit 1
    fi

    if ! umount "${UPDATE_ROOT_MNT}"; then
        echo "Error: failed to unmount '${UPDATE_ROOT_MNT}', update image may not be mounted.".
        exit 1
    fi

    TOOLBOX_MNT="$(mktemp -d -t "${TOOLBOX_MNT_TEMPLATE}.XXXXXXXX")"

    if ! mount "${UPDATE_FILE_SRC_DIR}/${TOOLBOX_FILENAME}" "${TOOLBOX_MNT}"; then
        echo "Error: failed to mount '${UPDATE_FILE_SRC_DIR}/${TOOLBOX_FILENAME}' to '${TOOLBOX_MNT}'".
        exit 1
    fi

    echo "Successfully mounted '${TOOLBOX_IMAGE}' on '${TOOLBOX_MNT}'."
}

cleanup()
{
    if mountpoint -q "${TOOLBOX_MNT:-}"; then
        echo "Cleanup: umount '${TOOLBOX_MNT}'."
        umount "${TOOLBOX_MNT}"
    fi

    if [ -d "${TOOLBOX_MNT:-}" ]; then
        rmdir "${TOOLBOX_MNT}"
    fi

    if mountpoint -q "${UPDATE_ROOT_MNT:-}"; then
        echo "Cleanup: umount '${UPDATE_ROOT_MNT}'."
        umount "${UPDATE_ROOT_MNT}"
    fi

    if [ -d "${UPDATE_ROOT_MNT:-}" ]; then
        rmdir "${UPDATE_ROOT_MNT}"
    fi
}

toolcheck()
{
    echo "Checking command availability."
    for cmd in ${CMDS}; do
        command -V "${cmd}"
    done
}

toolbox_run_update()
{
    echo "Running toolbox update ..."

    if [ ! -x "${TOOLBOX_MNT}/${SYSTEM_UPDATE_ENTRYPOINT}" ]; then
        echo "Error: Toolbox update script is not executable: '${TOOLBOX_MNT}/${SYSTEM_UPDATE_ENTRYPOINT}'."
        exit 1
    fi

    if ! "${TOOLBOX_MNT}/${SYSTEM_UPDATE_ENTRYPOINT}" "${TOOLBOX_MNT}" "${UPDATE_IMAGE}" "${TARGET_STORAGE_DEVICE}"; then
        echo "Error: Failed to run toolbox update '${TOOLBOX_MNT}/${SYSTEM_UPDATE_ENTRYPOINT}' '${UPDATE_IMAGE}' '${TARGET_STORAGE_DEVICE}'."
        exit 1
    fi

    echo "Toolbox completed."
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ "${#}" -lt 2 ]; then
        echo "Error: Missing arguments."
        usage
        exit 1
    fi

    if [ "${#}" -gt 2 ]; then
        echo "Error: Too many arguments."
        usage
        exit 1
    fi

    UPDATE_IMAGE="${1}"
    TARGET_STORAGE_DEVICE="${2}"

    if [ ! -f "${UPDATE_IMAGE}" ]; then
        echo "Error - Update image: '${UPDATE_IMAGE}' is missing."
        usage
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error: Update failed: '${TARGET_STORAGE_DEVICE}' is not a block device."
        usage
        exit 1
    fi

    toolcheck
    prepare
    toolbox_run_update
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
