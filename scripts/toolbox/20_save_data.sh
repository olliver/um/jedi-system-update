#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# Common directory variables
SYSCONFDIR="${SYSCONFDIR:-/etc}"

# Script mandatory configuration settings
UPDATE_SAVE_MNT="${UPDATE_SAVE_MNT:-}"
TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"

# Script optional configuration settings
SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update/}"
UPDATE_ROOT_MNT="${UPDATE_ROOT_MNT:-}"

TARGET_MNT_TEMPLATE="um-save_mount"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Save all data per '.keep' files from the TARGET_STORAGE_DEVICE."
    echo "Two data sources are supported, the local SYSTEM_UPDATE_CONF_DIR and"
    echo "a secondary prefixed by UPDATE_ROOT_MNT. Both are optional."
    echo "  -h Print this help text and exit"
    echo ""
    echo "All program parameters are passed along via the following environment variables."
    echo "  [UPDATE_ROOT_MNT], additional root device that contains a SYSTEM_UPDATE_CONF_DIR directory containing .keep files"
    echo "  [SYSTEM_UPDATE_CONF_DIR], directory that contains files ending with .keep extension containing files and directories to be saved up"
    echo "  [TARGET_STORAGE_DEVICE], the target storage device"
    echo "  [UPDATE_SAVE_MNT], location that will hold the created archives"
}

cleanup()
{
    if [ -f "${KEEP_LIST:-}" ]; then
        unlink "${KEEP_LIST}"
    fi

    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${TARGET_MNT:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${TARGET_MNT}"; then
            if ! umount "${TARGET_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${TARGET_MNT}" ] && \
           [ -z "${TARGET_MNT##*${TARGET_MNT_TEMPLATE}*}" ]; then
            if ! rmdir "${TARGET_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

prepare()
{
    KEEP_LIST="$(mktemp)"
    TARGET_MNT="$(mktemp -d -t "${TARGET_MNT_TEMPLATE}.XXXXXX")"
}

save_data()
{
    echo "Saving data ..."

    for keep in "${SYSTEM_UPDATE_CONF_DIR}/"*".keep" \
                "${UPDATE_ROOT_MNT}/${SYSTEM_UPDATE_CONF_DIR}/"*".keep"; do
        if [ ! -f "${keep}" ]; then
            continue
        fi

        # Tar likes its exclude list to be relative, so replace the initial
        # '/' on a line with './'.
        sed "s|^/|./|" "${keep}" >> "${KEEP_LIST}"
    done

    if [ ! -f "${KEEP_LIST}" ]; then
        echo "No '.keep' files found in '${SYSTEM_UPDATE_CONF_DIR}/' or '${UPDATE_ROOT_MNT}/${SYSTEM_UPDATE_CONF_DIR}/', nothing to save."
        exit 0
    fi

    for partition in "${TARGET_STORAGE_DEVICE}p"*; do
        if [ ! -b "${partition}" ]; then
            continue
        fi
        if ! mount -t auto "${partition}" "${TARGET_MNT}"; then
            echo "Unable to mount partition: '${partition}', skipping."
            continue
        fi

        update_target="${TARGET_MNT}"

        # When using overlay's, the actual data is stored in the 'upper'
        # sub-directory, as such, we use that as our 'root' directory.
        if [ -d "${TARGET_MNT}/upper" ]; then
            update_target="${TARGET_MNT}/upper"
        fi

        update_archive="${UPDATE_SAVE_MNT}/$(basename "${partition}").tar.bz2"
        tar --ignore-failed-read -cjf "${update_archive}" -C "${update_target}" --files-from "${KEEP_LIST}"

        umount "${partition}"

        tar -tjf "${update_archive}"
        echo "Successfully saved data in '${update_archive}'."
    done

    echo "Done saving data."
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Missing or invalid <TARGET_STORAGE_DEVICE>."
        usage
        exit 1
    fi

    if [ ! -d "${UPDATE_SAVE_MNT}" ]; then
        echo "Missing directory to save files to, cannot continue. '${UPDATE_SAVE_MNT}'"
        usage
        exit 1
    fi

    prepare
    save_data
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
