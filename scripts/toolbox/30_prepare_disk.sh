#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# common directory variables
SYSCONFDIR="${SYSCONFDIR:-/etc}"

# system_update wide configuration settings with default values
SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update}"
PARTITION_TABLE_FILE="${PARTITION_TABLE_FILE:-jedi_emmc_sfdisk.table}"
TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"
# end system_update wide configuration settings

BOOT_PARTITION_MIN_SIZE="32768" # 16MiB
BOOT_PARTITION_START="2048"
OTHER_PARTITION_MIN_SIZE="78124" # ~38MiB is the minimum for F2FS


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Prepare the target TARGET_STORAGE_DEVICE to a predefined disk layout."
    echo "  -d <TARGET_STORAGE_DEVICE>, the target storage device for the update (mandatory)"
    echo "  -h Print this help text and exit"
    echo "  -t <PARTITION_TABLE_FILE>, Partition table file (mandatory)"
    echo "Note: the PARTITION_TABLE_FILE and TARGET_STORAGE_DEVICE arguments can also be passed by"
    echo "adding them to the scripts runtime environment."
    echo "Warning: This script is destructive and will destroy your data."
}

is_integer()
{
    test "${1}" -eq "${1}" 2> /dev/null
}

is_comment()
{
    test -z "${1%%#*}"
}

verify_partition_file()
{
    cwd="$(pwd)"
    cd "${SYSTEM_UPDATE_CONF_DIR}" # change to update dir because path is hardcoded in the sha512 output.
    sha512sum -csw "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}.sha512" || return 1
    cd "${cwd}"
}

# Returns 0 when resize is needed and 1 if not needed.
is_resize_needed()
{
    # Produces a comma separated line containing only the start integer and
    # size integer of a partition whilst ignoring any leading zero's or empty
    # partitions (size=0), e.g. "2048,16384".
    FIND_PARTITION_DATA='s|.*start= *\([1-9][[:digit:]]*\).*size= *0*\([1-9][[:digit:]]*\).*|\1,\2|p'

    PARTITION_TABLE_FILE_DATA="$(sed -n "${FIND_PARTITION_DATA}" "${SYSTEM_UPDATE_CONF_DIR:?}/${PARTITION_TABLE_FILE}" | sort -n)"
    PARTITION_TABLE_DISK_DATA="$(sfdisk --quiet --dump "${TARGET_STORAGE_DEVICE}" | sed -n "${FIND_PARTITION_DATA}" | sort -n)"

    if [ "${PARTITION_TABLE_FILE_DATA}" != "${PARTITION_TABLE_DISK_DATA}" ]; then
        return 0
    fi

    return 1
}

partition_sync()
{
    i=10
    while [ "${i}" -gt 0 ]; do
        if partprobe "${TARGET_STORAGE_DEVICE}"; then
            return
        fi

        echo "Partprobe failed, retrying."
        sleep 1

        i=$((i - 1))
    done

    echo "Partprobe failed, giving up."

    return 1
}

partitions_format()
{
    # Parse the output of sfdisk and temporally expand the Input Field Separator
    # with ':=,' and treat them as whitespaces, in other words, ignore them.
    sfdisk --quiet --dump "${TARGET_STORAGE_DEVICE}" | \
    while IFS="${IFS}:=," read -r disk_device _ disk_start _; do
        while IFS="${IFS}:=," read -r table_device _ table_start _ _ _ _ _ table_name _; do
            if [ -z "${disk_start}" ] || [ -z "${table_start}" ] || \
               [ "${disk_start}" != "${table_start}" ]; then
                continue
            fi

            if [ ! -b "${disk_device}" ]; then
                echo "Error: '${disk_device}' is not a block device, cannot continue"
                exit 1
            fi

            if [ -z "${table_name}" ]; then
                echo "Error: partition label for '${table_device}' is empty"
                exit 1
            fi

            if mountpoint -q "${disk_device}"; then
                umount "${disk_device}"
            fi

            # Get the partition number from the device. e.g. /dev/loop0p1 -> p1
            # by grouping p with 1 or more digits and only printing the match,
            # with | being used as the command separator.
            # and then format the partition. If the partition was already valid,
            # just resize the existing one. If fsck or resize fails, reformat.
            partition="$(echo "${disk_device}" | sed -rn 's|.*(p[[:digit:]]+$)|\1|p')"
            if fstype="$(blkid -o value -s TYPE "${TARGET_STORAGE_DEVICE}${partition}")"; then
                echo "Attempting to resize partition ${TARGET_STORAGE_DEVICE}${partition}"
                case "${fstype}" in
                ext4)
                    fsck_cmd="fsck.ext4 -f -y"
                    fsck_ret_ok="1"
                    mkfs_cmd="mkfs.ext4 -F -L ${table_name} -O ^extents,^64bit"
                    resize_cmd="resize2fs"
                    ;;
                f2fs)
                    fsck_cmd="fsck.f2fs -f -p -y"
                    fsck_ret_ok="0"
                    mkfs_cmd="mkfs.f2fs -f -l ${table_name}"
                    resize_cmd="resize.f2fs"
                    ;;
                esac

                # In some cases of fsck, other values then 0 are acceptable,
                # as such we need to capture the return value or else set -u
                # will trigger eval as a failure and abort the script.
                fsck_status="$(eval "${fsck_cmd}" "${TARGET_STORAGE_DEVICE}${partition}" 1> /dev/null; echo "${?}")"
                if [ "${fsck_ret_ok}" -ge "${fsck_status}" ] && \
                   ! eval "${resize_cmd}" "${TARGET_STORAGE_DEVICE}${partition}"; then
                        echo "Resize failed, formatting instead."
                        eval "${mkfs_cmd}" "${TARGET_STORAGE_DEVICE}${partition}"
                fi
            else
                echo "Formatting ${TARGET_STORAGE_DEVICE}${partition}"
                if [ "${disk_start}" -eq "${BOOT_PARTITION_START}" ]; then
                    mkfs_cmd="mkfs.ext4 -F -L ${table_name} -O ^extents,^64bit"
                else
                    mkfs_cmd="mkfs.f2fs -f -l ${table_name}"
                fi

                eval "${mkfs_cmd}" "${TARGET_STORAGE_DEVICE}${partition}"
            fi
        done < "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}"
    done
}

partition_resize()
{
    sfdisk --quiet "${TARGET_STORAGE_DEVICE}" < "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}"
}

partition_table_sanity_check()
{
    boot_partition_available=false

    # sfdisk returns size in blocks, * (1024 / 512) converts to sectors
    target_disk_end="$(($(sfdisk --quiet --show-size "${TARGET_STORAGE_DEVICE}" 2> /dev/null) * 2))"

    # Temporally expand the Input Field Separator with ':=,' and treat them
    # as whitespaces, in other words, ignore them.
    # Allow for multiple reads on the same file;
    # shellcheck disable=SC2094
    while IFS="${IFS}:=," read -r device _ start _ size _; do
        if [ -z "${device}" ] || is_comment "${device}" || \
           ! is_integer "${start}" || ! is_integer "${size}"; then
            continue
        fi

        if [ "${start}" -eq "${BOOT_PARTITION_START}" ]; then
            boot_partition_available=true

            if [ "${size}" -lt "${BOOT_PARTITION_MIN_SIZE}" ]; then
                echo "Boot partition is to small (${size} < ${BOOT_PARTITION_MIN_SIZE}), cannot continue."
                exit 1
            fi
        else
            if [ "${size}" -lt "${OTHER_PARTITION_MIN_SIZE}" ]; then
                echo "Other partition '${device}' is to small (${size} < ${OTHER_PARTITION_MIN_SIZE}), cannot continue."
                exit 1
            fi
        fi

        partition_end="$((start + size))"
        if [ "${partition_end}" -gt "${target_disk_end}" ]; then
            echo "Partition '${device}' is beyond the size of the disk (${partition_end} > ${target_disk_end}), cannot continue."
            exit 1
        fi

        identical_found="0"
        while IFS="${IFS}:=," read -r verify_device _ verify_start _ verify_size _; do
            if [ -z "${verify_device}" ] || \
               is_comment "${verify_device}" || \
               ! is_integer "${verify_start}" || ! is_integer "${verify_size}"; then
                continue
            fi

            if [ "${device}" = "${verify_device}" ] || \
               [ "${start}" = "${verify_start}" ]; then
                identical_found="$((identical_found + 1))"
                continue
            fi

            if [ "${start}" -gt "${verify_start}" ] && \
               [ "${start}" -lt "$((verify_start + verify_size))" ]; then
                echo "Overlapping partitions detected (${device}:${start} ${verify_device}:${verify_start}-$((verify_start + verify_size))), cannot continue."
                exit 1
            fi
        done < "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}"

        if [ "${identical_found}" -ne "1" ]; then
            echo "Invalid entries found for '${device}'."
            exit 1
        fi
    done < "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}"

    if ! "${boot_partition_available}"; then
        echo "Error, no boot partition available, cannot continue."
        exit 1
    fi
}

main()
{
    while getopts ":d:ht:" options; do
        case "${options}" in
        d)
            TARGET_STORAGE_DEVICE="${OPTARG}"
            ;;
        h)
            usage
            exit 0
            ;;
        t)
            PARTITION_TABLE_FILE="${OPTARG}"
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ -z "${PARTITION_TABLE_FILE}" ]; then
        echo "Missing mandatory option <PARTITION_TABLE_FILE>."
        usage
        exit 1
    fi

    if [ -z "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Missing mandatory argument <TARGET_STORAGE_DEVICE>."
        usage
        exit 1
    fi

    if [ ! -f "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}" ]; then
        echo "Partition table file '${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}' does not exist, cannot continue."
        exit 1
    fi

    if [ ! -f "${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}.sha512" ]; then
        echo "Partition table checksum file '${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}.sha512' does not exist, cannot continue."
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error, block device '${TARGET_STORAGE_DEVICE}' does not exist."
        exit 1
    fi

    if ! verify_partition_file; then
        echo "Error: partition file crc error, cannot continue."
        exit 1
    fi

    if ! is_resize_needed; then
        echo "Partition resize not required."
        exit 0
    fi

    partition_table_sanity_check
    partition_resize
    partition_sync
    partitions_format
}

main "${@}"

exit 0
