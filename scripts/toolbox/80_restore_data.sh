#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# Script mandatory configuration settings
UPDATE_SAVE_MNT="${UPDATE_SAVE_MNT:-}"
TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"

TARGET_MNT_TEMPLATE="um-restore_mount"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Restore all data saved to UPDATE_SAVE_MNT to the TARGET_STORAGE_DEVICE."
    echo "  -h Print this help text and exit"
    echo ""
    echo "All program parameters are passed along via the following environment variables."
    echo "  [TARGET_STORAGE_DEVICE] the target storage device"
    echo "  [UPDATE_SAVE_MNT] location that holds the archives to restore"
}

cleanup()
{
    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${TARGET_MNT:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${TARGET_MNT}"; then
            if ! umount "${TARGET_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${TARGET_MNT}" ] && \
           [ -z "${TARGET_MNT##*${TARGET_MNT_TEMPLATE}*}" ]; then
            if ! rmdir "${TARGET_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

prepare()
{
    TARGET_MNT="$(mktemp -d -t "${TARGET_MNT_TEMPLATE}.XXXXXX")"
}

restore_data()
{
    echo "Restoring data ..."

    restore_count=0
    for archive in "${UPDATE_SAVE_MNT}/"*".tar.bz2"; do
        if [ ! -f "${archive}" ]; then
            continue
        fi

        partition="$(dirname "${TARGET_STORAGE_DEVICE}")/$(basename "${archive%.tar.bz2}")"
        if [ ! -b "${partition}" ]; then
            continue
        fi

        if ! tar -tjf "${archive}"; then
            echo "Restoration data not possible, archive '${archive}' corrupt."
            exit 1
        fi

        if ! mount -t auto "${partition}" "${TARGET_MNT}"; then
            echo "Unable to mount partition: '${partition}', skipping."
            continue
        fi

        update_target="${TARGET_MNT}"

        # When using overlay's, the actual data is stored in the 'upper'
        # sub-directory, as such, we use that as our 'root' directory.
        if [ -d "${TARGET_MNT}/upper" ]; then
            update_target="${TARGET_MNT}/upper"
        fi

        if ! tar -xjf "${archive}" -C "${update_target}"; then
            echo "Error restoring data."
            exit 1
        fi

        umount "${partition}"

        echo "  Restoration of '${archive}' to '${TARGET_MNT}' successful."
        restore_count="$((restore_count + 1))"
    done

    if [ "${restore_count}" -eq 0 ]; then
        echo "  Nothing to restore."
    fi

    echo "done."
}

main()
{
    while getopts ":d:hk:s:" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Missing or invalid <TARGET_STORAGE_DEVICE>."
        usage
        exit 1
    fi

    if [ ! -d "${UPDATE_SAVE_MNT}" ]; then
        echo "Missing directory to restore files from, cannot continue. '${UPDATE_SAVE_MNT}'"
        usage
        exit 1
    fi

    prepare
    restore_data
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
