#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# Common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SYSCONFDIR="${SYSCONFDIR:-/etc}"

CMDS=" \
    [ \
    chroot \
    echo \
    eval \
    exit \
    mount \
    mountpoint \
    shift \
    tr \
    umount \
"
# Script mandatory arguments
# The mount point of the update toolbox, used as chroot root.
TOOLBOX_MNT=""
# The target update storage device
TARGET_STORAGE_DEVICE=""
DEFAULT_IMAGE_NAME="/tmp/um-update.swu"
UPDATE_IMAGE="${UPDATE_IMAGE:-${DEFAULT_IMAGE_NAME}}"

# Toolbox execution environment arguments
# The directory in the chroot environment containing the system update and configuration files.
SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update/}"
SYSTEM_UPDATE_SCRIPT_DIR="${SYSTEM_UPDATE_SCRIPT_DIR:-${LIBEXECDIR}/jedi_system_update.d/}"
# The partition table file to use, default jedi_emmc_sfdisk.table, should exist in the system update dir.
PARTITION_TABLE_FILE="${PARTITION_TABLE_FILE:-jedi_emmc_sfdisk.table}"
# The directory in the chroot environment containing the source update files.
UPDATE_ROOT_MNT="/mnt/update_root_mnt"
# Directory containing data file archives for data save/restore.
UPDATE_SAVE_MNT="/mnt/update_save_mnt"


usage()
{
    echo "Usage: ${0} [OPTIONS] <TOOLBOX_MNT> <UPDATE_IMAGE> <TARGET_STORAGE_DEVICE>"
    echo "This is the update entry point script, it is responsible for setting up the"
    echo "environment in which the update toolbox can be used to configure and update"
    echo "the firmware."
    echo "  -h Print this help text and exit"
}

cleanup()
{
    if mountpoint -q "${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}"; then
        echo "Cleaning up, unmount: '${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}'."
        umount "${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}"
    fi

    if mountpoint -q "${TOOLBOX_MNT}/${UPDATE_SAVE_MNT}"; then
        echo "Cleaning up, unmount: '${TOOLBOX_MNT}/${UPDATE_SAVE_MNT}'."
        umount "${TOOLBOX_MNT}/${UPDATE_SAVE_MNT}"
    fi

    if mountpoint -q "${TOOLBOX_MNT}/proc"; then
        echo "Cleaning up, unmount: '${TOOLBOX_MNT}/proc'."
        umount "${TOOLBOX_MNT}/proc"
    fi

    if mountpoint -q "${TOOLBOX_MNT}/dev"; then
        echo "Cleaning up, unmount: '${TOOLBOX_MNT}/dev'."
        umount "${TOOLBOX_MNT}/dev"
    fi

    if mountpoint -q "${TOOLBOX_MNT}/tmp"; then
        echo "Cleaning up, unmount: '${TOOLBOX_MNT}/tmp'."
        umount "${TOOLBOX_MNT}/tmp"
    fi
}

prepare()
{
    echo "Preparing update..."

    if ! mountpoint -q "${TOOLBOX_MNT}/proc"; then
        mount -t proc none "${TOOLBOX_MNT}/proc"
    fi

    if ! mountpoint -q "${TOOLBOX_MNT}/dev"; then
        mount -t devtmpfs none "${TOOLBOX_MNT}/dev"
    fi

    if ! mountpoint -q "${TOOLBOX_MNT}/tmp"; then
        mount -t tmpfs none "${TOOLBOX_MNT}/tmp"
    fi

    if ! mount -t tmpfs none "${TOOLBOX_MNT}/${UPDATE_SAVE_MNT}"; then
        echo "Error, update failed: unable to setup save directory: '${TOOLBOX_MNT}/${UPDATE_SAVE_MNT}' cannot be mounted."
        exit 1
    fi
}

toolcheck()
{
    echo "Checking command availability."
    for cmd in ${CMDS}; do
        command -V "${cmd}"
    done
}

mount_firmware_update_file()
{
    echo "Mounting firmware update file."

    if [ ! -r "${UPDATE_IMAGE}" ]; then
        echo "Error, update failed: '${UPDATE_IMAGE}' not found."
        exit 1
    fi

    if [ ! -d "${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}" ]; then
        echo "Error, update failed: source update directory: '${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}' does not exist."
        exit 1
    fi

    if ! mount "${UPDATE_IMAGE}" "${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}"; then
        echo "Error, unable to mount '${UPDATE_IMAGE}' to '${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}'."
        exit 1
    fi

    echo "Successfully mounted '${UPDATE_IMAGE}' to '${TOOLBOX_MNT}/${UPDATE_ROOT_MNT}'."
}

perform_update()
{
    echo "Performing update..."
    chroot_environment=" \
        PARTITION_TABLE_FILE=${PARTITION_TABLE_FILE} \
        SYSTEM_UPDATE_CONF_DIR=${SYSTEM_UPDATE_CONF_DIR} \
        SYSTEM_UPDATE_SCRIPT_DIR=${SYSTEM_UPDATE_SCRIPT_DIR} \
        TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}\
        UPDATE_ROOT_MNT=${UPDATE_ROOT_MNT} \
        UPDATE_SAVE_MNT=${UPDATE_SAVE_MNT} \
    "

    echo "chroot script execution additional environment: "
    echo "${chroot_environment}" | tr -s "${IFS}" '\n'

    for script in "${TOOLBOX_MNT}/${SYSTEM_UPDATE_SCRIPT_DIR}/"[0-9][0-9]_*.sh; do
        if [ ! -x "${script}" ]; then
            continue
        fi

        script_to_execute="${script#"${TOOLBOX_MNT}"}"
        echo "executing: ${script_to_execute}"
        eval "${chroot_environment}" chroot "${TOOLBOX_MNT}" "${script_to_execute}"
    done

    echo "Successfully performed update."
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ "${#}" -lt 3 ]; then
        echo "Missing arguments."
        usage
        exit 1
    fi

    if [ "${#}" -gt 3 ]; then
        echo "Too many arguments."
        usage
        exit 1
    fi

    TOOLBOX_MNT="${1}"
    UPDATE_IMAGE="${2}"
    TARGET_STORAGE_DEVICE="${3}"

    if [ ! -d "${TOOLBOX_MNT}" ]; then
        echo "Error, update failed: ${TOOLBOX_MNT} is not a directory."
        exit 1
    fi

    if [ ! -f "${UPDATE_IMAGE}" ]; then
        echo "Error, update failed: '${UPDATE_IMAGE}' is missing."
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error, update failed: '${TARGET_STORAGE_DEVICE}' is not a block device."
        exit 1
    fi

    if [ ! -d "${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR}" ]; then
        echo "Error, update failed: ${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR} is not a directory."
        exit 1
    fi

    if [ ! -f "${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}" ]; then
        echo "Error, update failed: '${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}' not found."
        exit 1
    fi

    if [ ! -f "${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}.sha512" ]; then
        echo "Error, update failed: '${TOOLBOX_MNT}/${SYSTEM_UPDATE_CONF_DIR}/${PARTITION_TABLE_FILE}.sha512' not found."
        exit 1
    fi

    toolcheck
    prepare
    mount_firmware_update_file
    perform_update
}

trap cleanup EXIT

main "${@}"

exit 0
