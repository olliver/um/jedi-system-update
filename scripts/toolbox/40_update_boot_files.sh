#!/bin/sh
#
# Copyright (C) 2017-2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <r.siudak@ultimaker.com>
# Copyright (C) 2017-2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"
UPDATE_ROOT_MNT="${UPDATE_ROOT_MNT:-}"

# TODO: See EMP-399 Hardcoded list of files
KERNEL_BIN="uImage-sun7i-a20-opinicus_v1"
SPLASH_SCREEN="umsplash.bmp.gz"
UBOOT_ENV="um_u-boot.env.bin"
UBOOT_ENV_START=557056
UBOOT_BIN="u-boot-sunxi-with-spl.bin"
UBOOT_START=8192

TARGET_BOOT_DIR_TEMPLATE="um_updater_target_boot_dir"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "This is on of the update scripts, which is responsible for updating the boot files,"
    echo "U-Boot and the U-Boot environment"
    echo "    -h   Print usage"
    echo ""
    echo "The following parameters are required to be passed along via environment variables."
    echo "    [TARGET_STORAGE_DEVICE], the target storage device"
    echo "    [UPDATE_ROOT_MNT], mount point for the firmware update image"
    echo "Warning: This script is destructive and will destroy your data."
}

prepare()
{
    for partition in "${TARGET_STORAGE_DEVICE}p"*; do
        label="$(blkid -s LABEL -o value "${partition}")"
        if [ "${label}" = "boot" ]; then
            boot_partition="${partition}"
        fi
        if [ "${label}" = "root" ]; then
            root_partition="${partition}"
        fi
    done

    if [ -z "${root_partition}" ]; then
        echo "Error: unable to find root partition."
        exit 1
    fi
    echo "Found root partition '${root_partition}'."

    rootfs_type="$(blkid -o value -s TYPE "${root_partition}")"

    if [ -z "${boot_partition}" ]; then
        echo "Error: unable to find boot partition."
        exit 1
    fi
    echo "Found boot partition '${boot_partition}'."

    TARGET_BOOT_DIR="$(mktemp -t -d "${TARGET_BOOT_DIR_TEMPLATE}.XXXXXX")"
    if ! mount "${boot_partition}" "${TARGET_BOOT_DIR}"; then
        echo "Error: unable to mount boot partition '${boot_partition}' on '${TARGET_BOOT_DIR}'."
        exit 1
    fi

    current_uboot="$(mktemp -t um_updater_current_uboot.XXXXXX)"
    current_uboot_env="$(mktemp -t um_updater_current_uboot_env.XXXXXX)"
}

cleanup()
{
    echo "Cleaning up."

    if [ -f "${current_uboot_env}" ]; then
        unlink "${current_uboot_env}"
    fi

    if [ -f "${current_uboot}" ]; then
        unlink "${current_uboot}"
    fi

    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${TARGET_BOOT_DIR:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Error: failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${TARGET_BOOT_DIR}"; then
            if ! umount "${TARGET_BOOT_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${TARGET_BOOT_DIR}" ] && \
           [ -z "${TARGET_BOOT_DIR##*${TARGET_BOOT_DIR_TEMPLATE}*}" ]; then
            if ! rmdir "${TARGET_BOOT_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

compare_file()
{
    src_file="${1}"
    target_file="${2}"

    if [ -L "${src_file}" ]; then
        src_link="$(readlink "${src_file}")"
        target_link="$(readlink "${target_file}")"

        if [ "${src_link}" = "${target_link:-}" ]; then
            return 0
        fi
    else
        if cmp "${src_file}" "${target_file}"; then
            return 0
        fi
    fi

    return 1
}

copy_file()
{
    src_file="${1}"
    target_file="${2}"

    retries=5

    if compare_file "${src_file}" "${target_file}"; then
        echo "Source file: '${src_file}' and target file: '${target_file}' are the same, no need to copy."
        return 0
    fi

    echo "Copying source file: '${src_file}' to target file: '${target_file}' ..."

    while [ "${retries}" -ne 0 ]; do
        if cp -a "${src_file}" "${target_file}" && \
           compare_file "${src_file}" "${target_file}"; then
            break
        fi

        retries="$((retries - 1))"
        sleep 1
    done

    if [ "${retries}" -eq 0 ]; then
        echo "Error: failed copying source file '${src_file}'."
        exit 1
    fi

    echo "ok"
}

update_boot_splash()
{
    if [ ! -f "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN}" ]; then
        echo "No source file to copy, skipping."
        return 0
    fi

    if cmp "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN}" "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}"; then
        echo "Boot splash ${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN}' and '${TARGET_BOOT_DIR}/${SPLASH_SCREEN}' are identical, skipping copy."
        return 0
    fi

    echo "Update splash screen."

    available_bytes="$(df -B1 -P "${boot_partition}" | tail -n1 | awk '{ print $4 }')"
    required_bytes="$(stat -c %s "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN}")"
    if [ -f "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}" ]; then
        available_bytes="$((available_bytes + $(stat -c %s "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}")))"
    fi

    if [ "${required_bytes}" -gt "${available_bytes}" ]; then
        echo "Skipping '${SPLASH_SCREEN}' copy, not enough space available."
        return 0
    fi

    copy_file "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN}" "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}"
}

update_boot_files()
{
    echo "Update boot files."

    # To make the risk of failure as small as possible the order is important here.
    echo "Removing all none mandatory boot files."
    find "${TARGET_BOOT_DIR:?}/" \
        -type f \
        ! -name "${KERNEL_BIN}" \
        ! -name "*.dtb" \
        ! -name "boot.scr" \
        -delete

    echo "Update device tree files."
    for file in "${UPDATE_ROOT_MNT}/boot/"*.dtb; do
         copy_file "${file}" "${TARGET_BOOT_DIR}/$(basename "${file}")"
    done

    echo "Update kernel."
    copy_file "${UPDATE_ROOT_MNT}/boot/${KERNEL_BIN}" "${TARGET_BOOT_DIR}/${KERNEL_BIN}"

    echo "Removing deprecated device tree files."
    for file in "${TARGET_BOOT_DIR}/"*".dt"*; do
        if [ ! -f "${UPDATE_ROOT_MNT}/boot/$(basename "${file}")" ]; then
            unlink "${file}"
        fi
    done

    echo "Update U-Boot boot command."
    if [ "${rootfs_type}" = "f2fs" ]; then
        # Rootfs type is f2fs, so we must be booting from eMMC.
        copy_file "${UPDATE_ROOT_MNT}/boot/boot_emmc.scr" "${TARGET_BOOT_DIR}/boot.scr"
    elif [ "${rootfs_type}" = "ext4" ]; then
        # We are likely running ext4, which is the case when we are running from mmc.
        copy_file "${UPDATE_ROOT_MNT}/boot/boot_mmc.scr" "${TARGET_BOOT_DIR}/boot.scr"
    else
        echo "Error: unsupported root filesystem type '${rootfs_type}'."
        exit 1
    fi
}

write_file_if_needed()
{
    backup_filename="${1}"
    original_filename="${2}"
    byte_offset="${3}"
    byte_size="$(stat -c "%s" "${original_filename}")"

    retries=5

    while [ "${retries}" -gt 0 ]; do
        echo "Reading: '${backup_filename}'."
        if ! dd \
               bs=1 \
               conv=notrunc,fsync \
               count="${byte_size}" \
               if="${TARGET_STORAGE_DEVICE}" \
               of="${backup_filename}" \
               skip="${byte_offset}" \
               1> /dev/null; then
            echo "Error: unable to read current '${backup_filename}' from '${TARGET_STORAGE_DEVICE}' at offset '${byte_offset}', retrying ..."
            retries="$((retries - 1))"
            sleep 1
            continue
        fi

        if cmp "${backup_filename}" "${original_filename}"; then
            echo "Source file: '${backup_filename}' and target file: '${original_filename}' are the same, no need to copy."
            break
        fi

        echo "Programming: '${original_filename}'."
        if ! dd \
               bs=1 \
               conv=notrunc,fsync \
               count="${byte_size}" \
               if="${original_filename}" \
               of="${TARGET_STORAGE_DEVICE}" \
               seek="${byte_offset}" \
               1> /dev/null; then
            retries="$((retries - 1))"
            echo "Error: failed to write '${original_filename}' to '${TARGET_STORAGE_DEVICE}' at offset '${byte_offset}', retrying ..."
            sleep 1
            continue
        fi
    done

    if [ "${retries}" -eq 0 ]; then
        echo "Error: attempting to write bootloader(env), cannot continue."
        exit 1
    fi

    echo "ok"
}

update_uboot()
{
    if [ ! -f "${UPDATE_ROOT_MNT}/boot/${UBOOT_BIN}" ]; then
        echo "Error: U-Boot binary '${UPDATE_ROOT_MNT}/boot/${UBOOT_BIN}' does not exist."
        exit 1
    fi

    echo "Updating U-Boot ..."
    write_file_if_needed "${current_uboot}" "${UPDATE_ROOT_MNT}/boot/${UBOOT_BIN}" "${UBOOT_START}"

    if [ ! -r "${UPDATE_ROOT_MNT}/boot/${UBOOT_ENV}" ]; then
        echo "Error: unable to read U-Boot binary '${UPDATE_ROOT_MNT}/boot/${UBOOT_ENV}'."
        exit 1
    fi

    echo "Updating U-Boot environment ..."
    write_file_if_needed "${current_uboot_env}" "${UPDATE_ROOT_MNT}/boot/${UBOOT_ENV}" "${UBOOT_ENV_START}"
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -d "${UPDATE_ROOT_MNT}" ]; then
        echo "Error: update root directory '${UPDATE_ROOT_MNT}' does not exist."
        exit 1
    fi

    if [ -z "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error: Missing argument <TARGET_STORAGE_DEVICE>."
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error: target storage device '${TARGET_STORAGE_DEVICE}' is not a block device."
        exit 1
    fi

    prepare
    update_boot_files
    update_uboot
    update_boot_splash
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
