#!/bin/sh

set -eu

# common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SYSTEM_UPDATE_SCRIPT_DIR="${LIBEXECDIR}/jedi_system_update.d"

TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"
UPDATE_IMAGE_DEFAULT_NAME="um-update.swu"
UPDATE_IMAGE="${UPDATE_IMAGE:-/tmp/${UPDATE_IMAGE_DEFAULT_NAME}}"
UPDATE_ROOT_MNT_TEMPLATE="um-update_root_mnt"


usage()
{
    echo "Usage: ${0} [OPTIONS] <UPDATE_IMAGE>"
    echo "This is the update entry point script, which is responsible for 'update' environment"
    echo "preparation. It mounts the update image and executes the system update scripts in"
    echo "sorted order."
    echo "  -h Print this help text and exit"
    echo ""
    echo "The following parameters are optional and can be passed along via environment variables."
    echo "  [UPDATE_IMAGE], the path to the update image file"
    echo "  [TARGET_STORAGE_DEVICE], the target storage device"
}

prepare()
{
    UPDATE_FILE_SRC_DIR="$(dirname "${UPDATE_IMAGE}")"

    if [ "$(basename "${UPDATE_IMAGE}")" != "${UPDATE_IMAGE_DEFAULT_NAME}" ]; then
        echo "Renaming '$(basename "${UPDATE_IMAGE}")' to '${UPDATE_IMAGE_DEFAULT_NAME}' to comply with preceding update script interface."
        mv "${UPDATE_IMAGE}" "${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_DEFAULT_NAME}"
        UPDATE_IMAGE="${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_DEFAULT_NAME}"
    fi

    UPDATE_ROOT_MNT="$(mktemp -t -d "${UPDATE_ROOT_MNT_TEMPLATE}".XXXXXX)"

    echo "Mounting '${UPDATE_IMAGE}' on: '${UPDATE_ROOT_MNT}'."
    if ! mount "${UPDATE_IMAGE}" "${UPDATE_ROOT_MNT}" 1> /dev/null; then
        echo "Error - Unable to mount '${UPDATE_IMAGE}', cannot continue."
        exit 1
    fi

    if [ -z "${TARGET_STORAGE_DEVICE}" ] || [ "${#TARGET_STORAGE_DEVICE}" -le 1 ]; then
        root_partition="$(findmnt -b -f -n -o SOURCE "/")"
        TARGET_STORAGE_DEVICE="${root_partition%p*}"
    fi
}

cleanup()
{
    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${UPDATE_ROOT_MNT:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${UPDATE_ROOT_MNT}"; then
            if ! umount "${UPDATE_ROOT_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${UPDATE_ROOT_MNT}" ] && \
           [ -z "${UPDATE_ROOT_MNT##*${UPDATE_ROOT_MNT_TEMPLATE}*}" ]; then
            if ! rmdir "${UPDATE_ROOT_MNT}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

run_update_scripts()
{
    for script in "${UPDATE_ROOT_MNT}/${SYSTEM_UPDATE_SCRIPT_DIR}/"[0-9][0-9]*".sh"; do
        if [ ! -x "${script}" ]; then
            echo "Warning - Update script '${script}', is not executable."
            continue
        fi

        echo "Executing update script: '${script}'."
        if ! eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
                  "UPDATE_IMAGE=${UPDATE_IMAGE}" \
                  "UPDATE_ROOT_MNT=${UPDATE_ROOT_MNT}" \
                  "${script}"; then
            echo "Error - Update script '${script}', returned with an error."
            exit 1
        fi
    done
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"


    if [ "${#}" -ge 1 ]; then
        UPDATE_IMAGE="${1}"
    fi

    if [ ! -f "${UPDATE_IMAGE}" ]; then
        echo "Error - Update image: '${UPDATE_IMAGE}' is missing."
        usage
        exit 1
    fi

    if [ ! -r "${UPDATE_IMAGE}" ]; then
        echo "Error - Cannot read update image: '${UPDATE_IMAGE}' possible permission problem."
        usage
        exit 1
    fi

    echo "Starting firmware update"
    prepare
    run_update_scripts
    cleanup
    echo "Successfully performed pre-update configuration, ready to reboot."
}

trap cleanup EXIT

main "${@}"

exit 0
