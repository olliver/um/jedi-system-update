#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

SIG_SIZE_BYTES="4"
SIG_SIZE_MAX="1048576"
UPDATE_FILE="${UPDATE_FILE:-/tmp/um-update.swu}"


usage()
{
    echo "Usage: ${0} [OPTIONS] <UPDATE FILE>"
    echo "  -h Print this help text and exit"
    echo ""
    echo "  [UPDATE_FILE], path to the software update file (defaults to '/tmp/um-update.swu')"
}

cleanup()
{
    if [ -f "${SIGNATURE}" ]; then
        unlink "${SIGNATURE}"
    fi
}

prepare()
{
    GPGV="$(command -v gpgv)"
    SIGNATURE="$(mktemp)"
}

verify_update()
{
    if [ ! -f "${UPDATE_FILE}" ]; then
        echo "Unable to read '${UPDATE_FILE}', cannot continue."
        exit 1
    fi

    SIG_SIZE="$(printf "%u" "0x$(tail -c "${SIG_SIZE_BYTES}" "${UPDATE_FILE}" | od -An -t x4 | tr -d " ")")"
    if [ "${SIG_SIZE}" -gt "${SIG_SIZE_MAX}" ] || [ "${SIG_SIZE}" -le 0 ]; then
        echo "Unexpected signature size '${SIG_SIZE}'."
        exit 1
    fi

    tail -c "$((SIG_SIZE + SIG_SIZE_BYTES))" "${UPDATE_FILE}" | \
        head -c "${SIG_SIZE}" > "${SIGNATURE}"

    echo "Extracted '${SIG_SIZE}' bytes as the signature."

    if ! head -c "-$((SIG_SIZE + SIG_SIZE_BYTES))" "${UPDATE_FILE}" | \
       "${GPGV}" "${SIGNATURE}" "-" 1> /dev/null; then
        echo "Error: Invalid or corrupt software update file."
        exit 1
    fi
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ "${#}" -ne 1 ]; then
        echo "Invalid parameter for <UPDATE FILE>."
        usage
        exit 1
    fi
    UPDATE_FILE="${1}"

    prepare
    verify_update
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
