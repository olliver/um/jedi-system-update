#!/bin/sh
#
# Copyright (C) 2017-2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <r.siudak@ultimaker.com>
# Copyright (C) 2017-2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

# HACK: This script is a hack which should be replaced with the
# proper solution suggested in EMP-483.

set -eu

TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"
UPDATE_ROOT_MNT="${UPDATE_ROOT_MNT:-}"

SPLASH_SCREEN="umsplash.bmp.gz"
SPLASH_SCREEN_UPDATE="um-update_splash.bmp.gz"

TARGET_BOOT_DIR_TEMPLATE="um_updater_target_boot_dir"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "This is on of the update scripts, which is responsible for updating the boot files,"
    echo "U-Boot and the U-Boot environment"
    echo "    -h   Print usage"
    echo ""
    echo "The following parameters are required to be passed along via environment variables."
    echo "    [TARGET_STORAGE_DEVICE], the target storage device"
    echo "    [UPDATE_ROOT_MNT], mount point for the firmware update image"
    echo "Warning: This script is destructive and will destroy your data."
}

prepare()
{
    for partition in "${TARGET_STORAGE_DEVICE}p"*; do
        label="$(blkid -s LABEL -o value "${partition}")"
        if [ "${label}" = "boot" ]; then
            boot_partition="${partition}"
        fi
    done

    if [ -z "${boot_partition}" ]; then
        echo "Error: unable to find boot partition."
        exit 1
    fi
    echo "Found root partition '${boot_partition}'."

    TARGET_BOOT_DIR="$(mktemp -t -d "${TARGET_BOOT_DIR_TEMPLATE}.XXXXXX")"
    if ! mount "${boot_partition}" "${TARGET_BOOT_DIR}"; then
        echo "Error: unable to mount boot partition '${boot_partition}' on '${TARGET_BOOT_DIR}'."
        exit 1
    fi
}

cleanup()
{
    echo "Cleaning up."

    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${TARGET_BOOT_DIR:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Error: failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${TARGET_BOOT_DIR}"; then
            if ! umount "${TARGET_BOOT_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${TARGET_BOOT_DIR}" ] && \
           [ -z "${TARGET_BOOT_DIR##*${TARGET_BOOT_DIR_TEMPLATE}*}" ]; then
            if ! rmdir "${TARGET_BOOT_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

update_boot_splash()
{
    if [ ! -f "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN_UPDATE}" ]; then
        echo "No source file to copy, skipping."
        return 0
    fi

    if cmp "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN_UPDATE}" "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}"; then
        echo "Boot splash '${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN_UPDATE}' and '${TARGET_BOOT_DIR}/${SPLASH_SCREEN}' are identical, skipping copy."
        return 0
    fi

    echo "Update splash screen."

    available_bytes="$(df -B1 -P "${boot_partition}" | tail -n1 | awk '{ print $4 }')"
    required_bytes="$(stat -c %s "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN_UPDATE}")"
    if [ -f "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}" ]; then
        available_bytes="$((available_bytes + $(stat -c %s "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}")))"
    fi

    if [ "${required_bytes}" -gt "${available_bytes}" ]; then
        echo "Skipping '${SPLASH_SCREEN_UPDATE}' copy, not enough space available."
        return 0
    fi

    cp -f "${UPDATE_ROOT_MNT}/boot/${SPLASH_SCREEN_UPDATE}" "${TARGET_BOOT_DIR}/${SPLASH_SCREEN}"
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -d "${UPDATE_ROOT_MNT}" ]; then
        echo "Error: update root directory '${UPDATE_ROOT_MNT}' does not exist."
        exit 1
    fi

    if [ -z "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error: Missing argument <TARGET_STORAGE_DEVICE>."
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Error: target storage device '${TARGET_STORAGE_DEVICE}' is not a block device."
        exit 1
    fi

    prepare
    update_boot_splash
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
