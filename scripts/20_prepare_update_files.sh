#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

TARGET_STORAGE_DEVICE="${TARGET_STORAGE_DEVICE:-}"
UPDATE_IMAGE="${UPDATE_IMAGE:-}"

# common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
SBINDIR="${EXEC_PREFIX}/sbin"

EXTERNAL_CLEAN_USER_PARTITION_COMMAND="${EXTERNAL_CLEAN_USER_PARTITION_COMMAND:-${SBINDIR}/clean_user_partition.sh}"

FIRMWARE_UPDATE_FILE="um-update.swu"
UPDATE_FILE_DEST_DIR_TEMPLATE="update_file_dest_dir"

required_bytes=""


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "This is a pre-update script, which is responsible for finding a suitable partition"
    echo "to place the update files. It will loop over all partitions and will place the files"
    echo "on the first found partition that has enough space available. If not partition can"
    echo "can be found it will free space on the user partition."
    echo "  -h   Print this help text and exit"
    echo "NOTE: this script requires root permissions to run."
    echo ""
    echo "The following parameters are required to be passed along via environment variables."
    echo "  [TARGET_STORAGE_DEVICE], the target storage device"
    echo "  [UPDATE_IMAGE], the path to the update image file"
}

prepare()
{
    UPDATE_FILE_DEST_DIR="$(mktemp -d -t "${UPDATE_FILE_DEST_DIR_TEMPLATE}.XXXXXX")"

    if [ -z "${TARGET_STORAGE_DEVICE}" ] || [ "${#TARGET_STORAGE_DEVICE}" -le 1 ]; then
        root_partition="$(findmnt -b -f -n -o SOURCE "/")"
        TARGET_STORAGE_DEVICE="${root_partition%p*}"
    fi

    for partition in "${TARGET_STORAGE_DEVICE}p"*; do
        label="$(blkid -s LABEL -o value "${partition}")"
        if [ "${label}" = "user" ]; then
            user_partition="${partition}"
        fi
    done

    required_bytes="$(stat -c %s "${UPDATE_IMAGE}")"
    echo "${required_bytes} bytes required for update files."
}

cleanup()
{
    # On slow media, umount and/or rmdir can fail with 'resource busy' errors.
    # To do our best with cleanup, attempt this a few times before giving up.
    retries=300
    while test -d "${UPDATE_FILE_DEST_DIR:-}"; do
        printf "Cleaning up target_mount: "
        if [ "${retries}" -le 0 ]; then
            echo "Failed to properly cleanup."
            exit 1
        fi

        if mountpoint -q "${UPDATE_FILE_DEST_DIR}"; then
            if ! umount "${UPDATE_FILE_DEST_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        if [ -d "${UPDATE_FILE_DEST_DIR}" ] && \
           [ -z "${UPDATE_FILE_DEST_DIR##*${UPDATE_FILE_DEST_DIR_TEMPLATE}*}" ]; then
            if ! rmdir "${UPDATE_FILE_DEST_DIR}"; then
                retries="$((retries - 1))"
                sleep 1
                continue
            fi
        fi

        echo "ok"
    done
}

user_partition_free_space()
{
    update_file_partition="${user_partition}"

    if [ -n "${update_file_partition##*${TARGET_STORAGE_DEVICE}*}" ]; then
        echo "Cannot continue to free space, '${update_file_partition}' is not a partition of '${TARGET_STORAGE_DEVICE}'."
        exit 1
    fi

    if [ -r "${EXTERNAL_CLEAN_USER_PARTITION_COMMAND}" ] && \
         [ -x "${EXTERNAL_CLEAN_USER_PARTITION_COMMAND}" ]; then

        if ! "${EXTERNAL_CLEAN_USER_PARTITION_COMMAND}" "${update_file_partition}" "${required_bytes}"; then
            echo "Failed to execute external cleanup script: '${EXTERNAL_CLEAN_USER_PARTITION_COMMAND}'."
        fi
    fi

    echo "Attempting to mount '${update_file_partition}'."
    if ! mount "${update_file_partition}" "${UPDATE_FILE_DEST_DIR}"; then
        echo "Update failed, cannot mount '${update_file_partition}'."
        exit 1
    fi

    total_bytes="$(df -B1 --output=size "${update_file_partition}" | tail -n 1)"
    if [ "${required_bytes}" -gt "${total_bytes}" ]; then
        echo "Update failed, requiring minimum of: '${required_bytes}' bytes and no more then '${total_bytes}' can be made available."
        exit 1
    fi

    available_bytes="$(df -B1 --output=avail "${update_file_partition}" | tail -n 1)"

    echo "Freeing space on the user data partition."
    while [ "${required_bytes}" -gt "${available_bytes}" ]; do
        oldest_file="$(find "${UPDATE_FILE_DEST_DIR}" -type f -printf "%T+ %p\\n" | sort | head -n 1 | cut -b32-)"
        echo "removing file: '${oldest_file}'"
        unlink "${oldest_file}"
        available_bytes="$(df -B1 --output=avail "${update_file_partition}" | tail -n 1)"
    done

    if ! umount "${UPDATE_FILE_DEST_DIR}"; then
        echo "Update failed, cannot unmount '${UPDATE_FILE_DEST_DIR}'."
        exit 1
    fi
}

copy_file_to_destination()
{
    if ! mount "${update_file_partition}" "${UPDATE_FILE_DEST_DIR}"; then
        echo "Update failed, cannot mount '${update_file_partition}'."
        exit 1
    fi

    if ! cmp "${UPDATE_IMAGE}" "${UPDATE_FILE_DEST_DIR}/${FIRMWARE_UPDATE_FILE}" && \
       ! cp "${UPDATE_IMAGE}" \
        "${UPDATE_FILE_DEST_DIR}/${FIRMWARE_UPDATE_FILE}"; then
        echo "Update failed, unable to copy file '${FIRMWARE_UPDATE_FILE}'."
        exit 1
    fi

    if ! umount "${UPDATE_FILE_DEST_DIR}"; then
        echo "Update failed, cannot unmount '${UPDATE_FILE_DEST_DIR}'."
        exit 1
    fi
}

find_update_file_partition()
{
    echo "Searching for partition with enough available space ..."
    for partition in "${TARGET_STORAGE_DEVICE}p"[0-9]; do
        if [ ! -b "${partition}" ]; then
            continue
        fi

        if ! mount "${partition}" "${UPDATE_FILE_DEST_DIR}"; then
            continue
        fi

        available_bytes="$(df -B1 --output=avail "${partition}" | tail -n 1)"

        if ! umount "${UPDATE_FILE_DEST_DIR}"; then
            echo "Update failed, cannot unmount '${UPDATE_FILE_DEST_DIR}'."
            exit 1
        fi

        if [ "${required_bytes}" -gt "${available_bytes}" ]; then
            continue
        fi

        echo "Found enough available space on '${partition}'."
        update_file_partition="${partition}"
        break
    done
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"


    if [ -z "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Missing argument <TARGET_STORAGE_DEVICE>."
        usage
        exit 1
    fi

    if [ ! -b "${TARGET_STORAGE_DEVICE}" ]; then
        echo "Target storage device: '${TARGET_STORAGE_DEVICE}' is not a valid block device."
        exit 1
    fi

    if [ -z "${UPDATE_IMAGE}" ]; then
        echo "Error: Missing argument <UPDATE_IMAGE>."
        usage
        exit 1
    fi

    if [ ! -f "${UPDATE_IMAGE}" ]; then
        echo "Error: '${UPDATE_IMAGE}' is not a (update) file."
        usage
        exit 1
    fi

    if [ "$(id -u)" -ne 0 ]; then
        echo "Warning: this script requires root permissions."
        echo "Run this script again with 'sudo ${0}'."
        echo "See ${0} -h for more info."
        exit 1
    fi

    prepare
    find_update_file_partition
    if [ -z "${update_file_partition:-}" ]; then
        echo "Did not find a partition with enough available space."
        user_partition_free_space
    fi
    copy_file_to_destination
    cleanup
}

trap cleanup EXIT

main "${@}"

exit 0
