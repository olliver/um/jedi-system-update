#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# shellcheck source=test/include/chroot_env.sh
. "test/include/chroot_env.sh"

# common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SYSCONFDIR="${SYSCONFDIR:-/etc}"

ARM_EMU_BIN="${ARM_EMU_BIN:-}"

SRC_DIR="$(pwd)"

SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update}"
SYSTEM_UPDATE_SCRIPT_DIR="${SYSTEM_UPDATE_SCRIPT_DIR:-${LIBEXECDIR}/jedi_system_update.d/}"
UPDATE_ROOT_MNT="/tmp/update_root_mnt"
TARGET_STORAGE_DEVICE=""

JEDI_PARTITION_TABLE_FILE_NAME="config/toolbox/jedi_emmc_sfdisk.table"
UPDATE_FILES_COMMAND="${SYSTEM_UPDATE_SCRIPT_DIR}/50_update_files.sh"

ULTIMAKER_VERSION_FILE="${SYSCONFDIR}/ultimaker_version"

STORAGE_DEVICE_IMG="storage_device.img"
BYTES_PER_SECTOR="512"
STORAGE_DEVICE_SIZE="7553024" # sectors, about 3.6 GiB

TEST_OUTPUT_FILE="$(mktemp -d)/test_results_$(basename "${0%.sh}").txt"
TEST_FIRMWARE_UPDATE_FILE="test_um-update.swu"
FIXTURES="${SRC_DIR}/test/fixtures"

SQUASH_MODIFY_DIR="/tmp/squash_modify_dir"
TOOLBOX_TEMPLATE="um-update-toolbox"

toolbox_image=""
toolbox_root_dir=""

exit_on_failure=false
result=0

test_disk_integrity()
{
    # All return codes not 0 should be considered an error, since prepare_disk
    # should have fixed any potential filesystem error.
    sfdisk -Vl "${TARGET_STORAGE_DEVICE}"
    fsck.ext4 -fn "${TARGET_STORAGE_DEVICE}p1"
    fsck.f2fs "${TARGET_STORAGE_DEVICE}p2"
    fsck.f2fs "${TARGET_STORAGE_DEVICE}p3"
}

create_dummy_storage_device()
{
    echo "Creating test image: '${STORAGE_DEVICE_IMG}'."

    fallocate -l "$((BYTES_PER_SECTOR * STORAGE_DEVICE_SIZE))" "${STORAGE_DEVICE_IMG}"

    echo "writing partition table:"

    sfdisk "${STORAGE_DEVICE_IMG}" < "${SRC_DIR}/${JEDI_PARTITION_TABLE_FILE_NAME}"

    echo "formatting partitions"

    TARGET_STORAGE_DEVICE="$(losetup --show --find --partscan "${STORAGE_DEVICE_IMG}")"

    mkfs.ext4 -L "boot" -q "${TARGET_STORAGE_DEVICE}p1"
    mkfs.f2fs -l "root" -q "${TARGET_STORAGE_DEVICE}p2"
    mkfs.f2fs -l "user" -q "${TARGET_STORAGE_DEVICE}p3"

    test_disk_integrity

    echo "Successfully created dummy storage device: '${TARGET_STORAGE_DEVICE}'."
}

setup()
{
    toolbox_root_dir="$(mktemp -d -t "${TOOLBOX_TEMPLATE}.XXXXXX")"
    setup_chroot_env "${toolbox_image}" "${toolbox_root_dir}"

    mkdir -p "${toolbox_root_dir}/${UPDATE_ROOT_MNT}"
    mkdir -p "${toolbox_root_dir}/${SQUASH_MODIFY_DIR}"

    mount "${FIXTURES}/${TEST_FIRMWARE_UPDATE_FILE}" "${toolbox_root_dir}/${UPDATE_ROOT_MNT}"

    create_dummy_storage_device
}

teardown()
{
    if mountpoint -q "${toolbox_root_dir}/${UPDATE_ROOT_MNT}"; then
        umount "${toolbox_root_dir}/${UPDATE_ROOT_MNT}"
    fi

    if [ -d "${toolbox_root_dir}/${UPDATE_ROOT_MNT}" ] && [ -z "${toolbox_root_dir##*${TOOLBOX_TEMPLATE}*}" ]; then
        rmdir "${toolbox_root_dir:?}/${UPDATE_ROOT_MNT}"
    fi

    if [ -d "${toolbox_root_dir}/${SQUASH_MODIFY_DIR}" ] && [ -z "${toolbox_root_dir##*${TOOLBOX_TEMPLATE}*}" ]; then
        rmdir "${toolbox_root_dir:?}/${SQUASH_MODIFY_DIR}"
    fi

    teardown_chroot_env "${toolbox_root_dir}"

    if [ -b "${TARGET_STORAGE_DEVICE}" ]; then
        losetup -d "${TARGET_STORAGE_DEVICE}"
        TARGET_STORAGE_DEVICE=""
    fi

    if [ -f "${STORAGE_DEVICE_IMG}" ]; then
        unlink "${STORAGE_DEVICE_IMG}"
    fi

    if [ -d "${toolbox_root_dir}" ] && [ -z "${toolbox_root_dir##*${TOOLBOX_TEMPLATE}*}" ]; then
        rmdir "${toolbox_root_dir}"
    fi
}

failure_exit()
{
    echo "Test exit per request."
    echo "When finished, the following is needed to cleanup!"
    echo "  sudo sh -c '\\"
    echo "    losetup -d '${TARGET_STORAGE_DEVICE}' && \\"

    failure_exit_chroot_env

    echo "    rmdir '${toolbox_root_dir}/${UPDATE_ROOT_MNT}' && \\"
    echo "    rmdir '${toolbox_root_dir}/${SQUASH_MODIFY_DIR}' && \\"
    echo "    rmdir '${toolbox_root_dir}'"
    echo "  '"
    exit "${result}"
}

cleanup()
{
    if "${exit_on_failure}"; then
        failure_exit
    else
        teardown
    fi
}

run_test()
{
    setup

    echo "________________________________________________________________________________"
    echo
    echo "Run: ${1}"
    echo
    echo
    if "${1}"; then
        echo "Result - OK"
        echo "OK    - ${1}" >> "${TEST_OUTPUT_FILE}"
    else
        echo "Result - ERROR"
        echo "ERROR - ${1}" >> "${TEST_OUTPUT_FILE}"
        result=1
        if "${exit_on_failure}"; then
            exit "${result}"
        fi
    fi
    echo "________________________________________________________________________________"

    teardown
}

test_missing_argument_update_rootfs_mount_nok()
{
    eval chroot "${toolbox_root_dir}" "${UPDATE_FILES_COMMAND}" -s '' -d "${TARGET_STORAGE_DEVICE}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_update_rootfs_mount_not_a_directory_nok()
{
    chroot_environment=" \
        UPDATE_ROOT_MNT=/tmp/not_existing_dir \
        TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE} \
    "

    eval "${chroot_environment}" chroot "${toolbox_root_dir}" "${UPDATE_FILES_COMMAND}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_invalid_block_device_nok()
{
    target_device="/dev/loop100"

    eval chroot "${toolbox_root_dir}" "${UPDATE_FILES_COMMAND}" -s "${UPDATE_ROOT_MNT}" -d "${target_device}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_no_ultimaker_software_found_nok()
{
    chroot_environment=" \
        TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE} \
        UPDATE_ROOT_MNT=${SQUASH_MODIFY_DIR} \
    "

    unsquashfs -d "${SQUASH_MODIFY_DIR}" -f "${FIXTURES}/${TEST_FIRMWARE_UPDATE_FILE}"
    unlink "${toolbox_root_dir}/${SQUASH_MODIFY_DIR}/${ULTIMAKER_VERSION_FILE}"

    eval "${chroot_environment}" chroot "${toolbox_root_dir}" "${UPDATE_FILES_COMMAND}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_update_files_ok()
{
    chroot_environment=" \
        TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE} \
        UPDATE_ROOT_MNT=${UPDATE_ROOT_MNT} \
    "

    eval "${chroot_environment}" chroot "${toolbox_root_dir}" "${UPDATE_FILES_COMMAND}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    update_target="$(mktemp -d -t "tmp_update_target.XXXXXX")"
    mount -t auto -v "${TARGET_STORAGE_DEVICE}p2" "${update_target}"

    # Note that with rsync --delete, we want to ensure both the '.keep' as
    # well as the '.discard' file are ignored.
    for exclude in "${SRC_DIR}/config/toolbox/"*".keep" \
                   "${SRC_DIR}/config/toolbox/"*".discard"; do
        if [ ! -f "${exclude}" ]; then
            continue
        fi

        exclude_list="${exclude_list:-} --exclude-from ${exclude}"
    done

    rsync -a -c -x --dry-run \
        "${toolbox_root_dir}/${UPDATE_ROOT_MNT}/" \
        "${update_target}/" \
        "${exclude_list}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    umount "${update_target}"

    if [ -z "${update_target##*tmp_update_target*}" ]; then
        rm -rf "${update_target:?}"
    fi
}

usage()
{
    echo "Usage:   ${0} [OPTIONS] <toolbox image file>"
    echo "  -e   Stop consecutive tests on failure without cleanup"
    echo "  -h   Print usage"
    echo "NOTE: This script requires root permissions to run."
}

while getopts ":eh" options; do
    case "${options}" in
    e)
        exit_on_failure=true
        ;;
    h)
        usage
        exit 0
        ;;
    :)
        echo "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo "Invalid option: -${OPTARG}."
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"

if [ "${#}" -ne 1 ]; then
    echo "Missing argument <toolbox image file>."
    usage
    exit 1
fi

toolbox_image="${*}"

if [ ! -r "${toolbox_image}" ]; then
    echo "Given toolbox image '${toolbox_image}' not found."
    usage
    exit 1
fi

if [ "$(id -u)" -ne 0 ]; then
    echo "Warning: this script requires root permissions."
    echo "Run this script again with 'sudo ${0}'."
    echo "See ${0} -h for more info."
    exit 1
fi

trap cleanup EXIT

run_test test_missing_argument_update_rootfs_mount_nok
run_test test_update_rootfs_mount_not_a_directory_nok
run_test test_invalid_block_device_nok
run_test test_no_ultimaker_software_found_nok
run_test test_update_files_ok

echo "________________________________________________________________________________"
echo "Test results '${TEST_OUTPUT_FILE}':"
echo
cat "${TEST_OUTPUT_FILE}"
echo "________________________________________________________________________________"

if [ "${result}" -ne 0 ]; then
   exit 1
fi

exit 0
