#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SYSTEM_UPDATE_SCRIPT_DIR="${LIBEXECDIR}/jedi_system_update.d"

CWD="$(dirname "${0}")"

SCRIPT_DIR="${CWD}/../scripts"
START_UPDATE_COMMAND="${SCRIPT_DIR}/pre_update.sh"
TARGET_STORAGE_DEVICE="a_storage_device"

TEST_OUTPUT_FILE="$(mktemp -d)/test_results_$(basename "${0%.sh}").txt"

FIRMWARE_UPDATE_FILE="um-update.swu"
TEST_RESULT_DIR_TEMPLATE="test_results_dir"
TEST_ROOTFS_DIR_TEMPLATE="test_rootfs_dir"
TEST_UPDATE_FILE_SRC_DIR_TEMPLATE="test_um-update"

exit_on_failure=false
result=0


make_dummy_squashfs()
{
    mksquashfs "${TEST_ROOTFS_DIR}" "${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" -comp xz
}

setup()
{
    TEST_RESULT_DIR="$(mktemp -d -t "${TEST_RESULT_DIR_TEMPLATE}.XXXXXXXX")"
    TEST_ROOTFS_DIR="$(mktemp -d -t "${TEST_ROOTFS_DIR_TEMPLATE}.XXXXXXXX")"
    TEST_UPDATE_FILE_SRC_DIR="$(mktemp -d -t "${TEST_UPDATE_FILE_SRC_DIR_TEMPLATE}.XXXXXXXX")"

    mkdir -p "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}"
}

teardown()
{
    if [ -d "${TEST_UPDATE_FILE_SRC_DIR}" ] && [ -z "${TEST_UPDATE_FILE_SRC_DIR##*${TEST_UPDATE_FILE_SRC_DIR_TEMPLATE}*}" ]; then
        rm -rf "${TEST_UPDATE_FILE_SRC_DIR:?}/"
    fi

    if [ -d "${TEST_ROOTFS_DIR}" ] && [ -z "${TEST_ROOTFS_DIR##*${TEST_ROOTFS_DIR_TEMPLATE}*}" ]; then
        rm -rf "${TEST_ROOTFS_DIR:?}/"
    fi

    if [ -d "${TEST_RESULT_DIR}" ] && [ -z "${TEST_RESULT_DIR##*${TEST_RESULT_DIR_TEMPLATE}*}" ]; then
        rm -rf "${TEST_RESULT_DIR:?}/"
    fi
}

failure_exit()
{
    echo "Test exit per request."
    echo "When finished, the following is needed to cleanup!"
    echo "  sudo sh -c '\\"
    echo "    rm -rf '${TEST_RESULT_DIR}' && \\"
    echo "    rm -rf '${TEST_ROOTFS_DIR}' && \\"
    echo "    rm -rf '${TEST_UPDATE_FILE_SRC_DIR}' && \\"
    echo "  '"
    exit "${result}"
}

cleanup()
{
    if "${exit_on_failure}"; then
        failure_exit
    else
        teardown
    fi
}

run_test()
{
    setup

    echo "________________________________________________________________________________"
    echo
    echo "Run: ${1}"
    echo
    echo
    if "${1}"; then
        echo "Result - OK"
        echo "OK    - ${1}" >> "${TEST_OUTPUT_FILE}"
    else
        echo "Result - ERROR"
        echo "ERROR - ${1}" >> "${TEST_OUTPUT_FILE}"
        result=1
        if "${exit_on_failure}"; then
            exit "${result}"
        fi
    fi
    echo "________________________________________________________________________________"

    teardown
}

test_execution_of_update_scripts_ok()
{
    cat > "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_first_update_script.sh" <<\
________________________________________________________________
#!/bin/sh
echo "executed: \$(basename \${0})" >> "${TEST_RESULT_DIR}/test.txt"
echo "target storage device: \${TARGET_STORAGE_DEVICE}" >> "${TEST_RESULT_DIR}/test.txt"
echo "update image file: \${UPDATE_IMAGE}" >> "${TEST_RESULT_DIR}/test.txt"
exit 0
________________________________________________________________

    chmod +x "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_first_update_script.sh"

    cat > "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/20_first_update_script.sh" <<\
________________________________________________________________
#!/bin/sh
echo "executed: \$(basename \${0})" >> "${TEST_RESULT_DIR}/test.txt"
echo "target storage device: \${TARGET_STORAGE_DEVICE}" >> "${TEST_RESULT_DIR}/test.txt"
echo "update image file: \${UPDATE_IMAGE}" >> "${TEST_RESULT_DIR}/test.txt"
exit 0
________________________________________________________________

    chmod +x "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/20_first_update_script.sh"

    cat > "${TEST_RESULT_DIR}/test_verify.txt" <<\
________________________________________________________________
executed: 10_first_update_script.sh
target storage device: ${TARGET_STORAGE_DEVICE}
update image file: ${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}
executed: 20_first_update_script.sh
target storage device: ${TARGET_STORAGE_DEVICE}
update image file: ${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}
________________________________________________________________

    make_dummy_squashfs

    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "UPDATE_IMAGE=${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" \
         "${START_UPDATE_COMMAND}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    diff "${TEST_RESULT_DIR}/test.txt" "${TEST_RESULT_DIR}/test_verify.txt"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    rm "${TEST_RESULT_DIR}/test.txt"

    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "${START_UPDATE_COMMAND}" "${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    diff "${TEST_RESULT_DIR}/test.txt" "${TEST_RESULT_DIR}/test_verify.txt"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi
}

test_preceding_update_script_interface_compliance_ok()
{
    cat > "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_test_update_file_re-name.sh" <<\
________________________________________________________________
#!/bin/sh

if [ ! -f "\${UPDATE_IMAGE}" ] || \
   [ ! -r "\${UPDATE_IMAGE}" ] || \
   [ -z "\${TARGET_STORAGE_DEVICE}" ]; then \
    exit 1
fi

exit 0
________________________________________________________________

    chmod +x "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_test_update_file_re-name.sh"

    make_dummy_squashfs
    firmware_update_file="um-update-that_should_be_renamed.swu"
    mv "${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" "${TEST_UPDATE_FILE_SRC_DIR}/${firmware_update_file}"

    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "UPDATE_IMAGE=${TEST_UPDATE_FILE_SRC_DIR}/${firmware_update_file}" \
         "${START_UPDATE_COMMAND}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi
}

test_preceding_update_script_fails_script_exits_nok()
{
    cat > "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_always_fails.sh" <<\
________________________________________________________________
#!/bin/sh

exit 1
________________________________________________________________

    chmod +x "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_always_fails.sh"

    make_dummy_squashfs

    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "UPDATE_IMAGE=${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" \
         "${START_UPDATE_COMMAND}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_update_image_not_found_script_exits_nok()
{
    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "UPDATE_FILE=${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" \
         "${START_UPDATE_COMMAND}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_something_fails_everything_is_cleaned_up()
{
    cat > "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_always_fails.sh" <<\
________________________________________________________________
#!/bin/sh

exit 0
________________________________________________________________

    chmod +x "${TEST_ROOTFS_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}/10_always_fails.sh"

    make_dummy_squashfs

    update_root_mnt="$(mktemp -d -t "update_root_mnt.XXXXXX")"

    test_result=0

    eval "TARGET_STORAGE_DEVICE=${TARGET_STORAGE_DEVICE}" \
         "UPDATE_FILE=${TEST_UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" \
         "${START_UPDATE_COMMAND}"

    if mountpoint -q "${update_root_mnt}"; then
        umount "${update_root_mnt}"
        test_result=1
    fi

    if [ -d "${update_root_mnt}" ] && [ -z "${update_root_mnt##update_root_mnt*}" ]; then
        rm -r "${update_root_mnt:?}"
        test_result=1
    fi

    return "${test_result}"
}

usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "  -e   Stop consecutive tests on failure without cleanup"
    echo "  -h   Print usage"
}

while getopts ":eh" options; do
    case "${options}" in
    e)
        exit_on_failure=true
        ;;
    h)
        usage
        exit 0
        ;;
    :)
        echo "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo "Invalid option: -${OPTARG}."
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"


trap cleanup EXIT

run_test test_execution_of_update_scripts_ok
run_test test_preceding_update_script_interface_compliance_ok
run_test test_preceding_update_script_fails_script_exits_nok
run_test test_update_image_not_found_script_exits_nok
run_test test_something_fails_everything_is_cleaned_up

echo "________________________________________________________________________________"
echo "Test results '${TEST_OUTPUT_FILE}':"
echo
cat "${TEST_OUTPUT_FILE}"
echo "________________________________________________________________________________"

if [ "${result}" -ne 0 ]; then
   exit 1
fi

exit 0
