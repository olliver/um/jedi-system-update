#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# shellcheck source=test/include/chroot_env.sh
. "test/include/chroot_env.sh"


# common directory variables
PREFIX="/usr"
EXEC_PREFIX="${PREFIX}"
SBINDIR="${EXEC_PREFIX}/sbin"
SYSCONFDIR="${SYSCONFDIR:-/etc}"

ARM_EMU_BIN="${ARM_EMU_BIN:-}"

SRC_DIR="$(pwd)"

SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update}"
START_UPDATE_COMMAND="${SBINDIR}/start_update.sh"
VERIFY_UPDATE_ROOTFS="/tmp/update_source"
TARGET_STORAGE_DEVICE=""

JEDI_PARTITION_TABLE_FILE_NAME="config/toolbox/jedi_emmc_sfdisk.table"

STORAGE_DEVICE_IMG="storage_device.img"
BYTES_PER_SECTOR="512"
STORAGE_DEVICE_SIZE="7553024" # sectors, about 3.6 GiB

TEST_FIRMWARE_UPDATE_FILE="test_um-update.swu"
FIXTURES="${SRC_DIR}/test/fixtures"
TEST_OUTPUT_FILE="$(mktemp -d)/test_results_$(basename "${0%.sh}").txt"

TOOLBOX_TEMPLATE="um-update-toolbox"
UPDATE_FILE_DIR_TEMPLATE="temp_update_file_dir"

toolbox_image=""
toolbox_root_dir=""
update_file_src_dir=""

exit_on_failure=false
result=0

test_disk_integrity()
{
    # All return codes not 0 should be considered an error, since prepare_disk
    # should have fixed any potential filesystem error.
    sfdisk -Vl "${TARGET_STORAGE_DEVICE}"
    fsck.ext4 -fn "${TARGET_STORAGE_DEVICE}p1"
    fsck.f2fs "${TARGET_STORAGE_DEVICE}p2"
    fsck.f2fs "${TARGET_STORAGE_DEVICE}p3"
}

create_dummy_storage_device()
{
    echo "Creating test image: '${STORAGE_DEVICE_IMG}'."

    fallocate -l "$((BYTES_PER_SECTOR * STORAGE_DEVICE_SIZE))" "${STORAGE_DEVICE_IMG}"

    echo "writing partition table:"

    sfdisk "${STORAGE_DEVICE_IMG}" < "${SRC_DIR}/${JEDI_PARTITION_TABLE_FILE_NAME}"

    echo "formatting partitions"

    TARGET_STORAGE_DEVICE="$(losetup --show --find --partscan "${STORAGE_DEVICE_IMG}")"

    mkfs.ext4 -L "boot" -q "${TARGET_STORAGE_DEVICE}p1"
    mkfs.f2fs -l "root" -q "${TARGET_STORAGE_DEVICE}p2"
    mkfs.f2fs -l "user" -q "${TARGET_STORAGE_DEVICE}p3"

    test_disk_integrity

    echo "Successfully created dummy storage device: '${TARGET_STORAGE_DEVICE}'."
}

setup()
{
    toolbox_root_dir="$(mktemp -d -t "${TOOLBOX_TEMPLATE}.XXXXXX")"
    setup_chroot_env "${toolbox_image}" "${toolbox_root_dir}"

    mkdir -p "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}"

    update_file_src_dir="$(mktemp -d -t "${UPDATE_FILE_DIR_TEMPLATE}.XXXXXX")"

    create_dummy_storage_device
}

teardown()
{
    teardown_chroot_env "${toolbox_root_dir}"

    if [ -b "${TARGET_STORAGE_DEVICE}" ]; then
        losetup -d "${TARGET_STORAGE_DEVICE}"
        TARGET_STORAGE_DEVICE=""
    fi

    if [ -d "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}" ] && \
       [ -z "${toolbox_root_dir##*${TOOLBOX_TEMPLATE}*}" ]; then
        rmdir "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}"
    fi

    if [ -f "${STORAGE_DEVICE_IMG}" ]; then
        unlink "${STORAGE_DEVICE_IMG}"
    fi

    if [ -d "${toolbox_root_dir}" ] && [ -z "${toolbox_root_dir##*${TOOLBOX_TEMPLATE}*}" ]; then
        rmdir "${toolbox_root_dir}"
    fi

    if [ -d "${update_file_src_dir}" ] && [ -z "${update_file_src_dir##*${UPDATE_FILE_DIR_TEMPLATE}*}" ]; then
        rm -rf "${update_file_src_dir:?}"
    fi
}

failure_exit()
{
    echo "Test exit per request."
    echo "When finished, the following is needed to cleanup!"
    echo "  sudo sh -c '\\"
    echo "    losetup -d '${TARGET_STORAGE_DEVICE}' && \\"
    echo "    rm -rf '${update_file_src_dir}/*' && \\"

    failure_exit_chroot_env

    echo "    rmdir '${toolbox_root_dir}'"
    echo "  '"
    exit "${result}"
}

cleanup()
{
    if "${exit_on_failure}"; then
        failure_exit
    else
        teardown
    fi
}

run_test()
{
    setup

    echo "________________________________________________________________________________"
    echo
    echo "Run: ${1}"
    echo
    echo
    if "${1}"; then
        echo "Result - OK"
        echo "OK    - ${1}" >> "${TEST_OUTPUT_FILE}"
    else
        echo "Result - ERROR"
        echo "ERROR - ${1}" >> "${TEST_OUTPUT_FILE}"
        result=1
        if "${exit_on_failure}"; then
            exit "${result}"
        fi
    fi
    echo "________________________________________________________________________________"

    teardown
}

test_update_firmware_image_corrupt_nok()
{
    cp "${FIXTURES}/${TEST_FIRMWARE_UPDATE_FILE}" "${update_file_src_dir}/um-update.swu"
    dd if=/dev/zero of="${update_file_src_dir}/um-update.swu" bs=1 count=32

    "${toolbox_root_dir}/${START_UPDATE_COMMAND}" "${toolbox_root_dir}" "${update_file_src_dir}" "${TARGET_STORAGE_DEVICE}"
    if [ "${?}" -eq 0 ]; then
        return 1
    fi
}

test_successful_update_ok()
{
    cp "${FIXTURES}/${TEST_FIRMWARE_UPDATE_FILE}" "${update_file_src_dir}/um-update.swu"
    "${toolbox_root_dir}/${START_UPDATE_COMMAND}" "${toolbox_root_dir}" "${update_file_src_dir}/um-update.swu" "${TARGET_STORAGE_DEVICE}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    update_target="$(mktemp -d)"
    mount -t auto -v "${TARGET_STORAGE_DEVICE}p2" "${update_target}"

    # Note that with rsync --delete, we want to ensure both the '.keep' as
    # well as the '.discard' file are ignored.
    for exclude in "${SRC_DIR}/config/toolbox/"*".keep" \
                   "${SRC_DIR}/config/toolbox/"*".discard"; do
        if [ ! -f "${exclude}" ]; then
            continue
        fi

        exclude_list="${exclude_list:-} --exclude-from ${exclude}"
    done

    mount "${FIXTURES}/${TEST_FIRMWARE_UPDATE_FILE}" "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}"

    rsync -a -c -x --dry-run \
        "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}/" "${update_target}/" \
        "${exclude_list}"
    if [ "${?}" -ne 0 ]; then
        return 1
    fi

    umount "${toolbox_root_dir}/${VERIFY_UPDATE_ROOTFS}"

    umount "${update_target}"

    if [ -z "${update_target##*/tmp/*}" ]; then
        rm -rf "${update_target:?}"
    fi

#    TODO EMP-449: verify boot files
}

usage()
{
    echo "Usage:   ${0} [OPTIONS] <toolbox image file>"
    echo "  -e   Stop consecutive tests on failure without cleanup"
    echo "  -h   Print usage"
    echo "NOTE: This script requires root permissions to run."
}

while getopts ":eh" options; do
    case "${options}" in
    e)
        exit_on_failure=true
        ;;
    h)
        usage
        exit 0
        ;;
    :)
        echo "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo "Invalid option: -${OPTARG}."
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"

if [ "${#}" -ne 1 ]; then
    echo "Missing argument <toolbox image file>."
    usage
    exit 1
fi

toolbox_image="${*}"

if [ ! -r "${toolbox_image}" ]; then
    echo "Given toolbox image '${toolbox_image}' not found."
    usage
    exit 1
fi

if [ "$(id -u)" -ne 0 ]; then
    echo "Warning: this script requires root permissions."
    echo "Run this script again with 'sudo ${0}'."
    echo "See ${0} -h for more info."
    exit 1
fi

trap cleanup EXIT

run_test test_update_firmware_image_corrupt_nok
run_test test_successful_update_ok

echo "________________________________________________________________________________"
echo "Test results '${TEST_OUTPUT_FILE}':"
echo
cat "${TEST_OUTPUT_FILE}"
echo "________________________________________________________________________________"

if [ "${result}" -ne 0 ]; then
   exit 1
fi

exit 0
