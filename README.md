# Introduction
This repository contains the jedi system update service. It is responsible for building a *toolbox* that is used
in the early boot stages responsible for system configuration and updating the root files system.
The other responsibility is preparing the system with necessary files so that it can properly execute the toolbox routines,
while the system is still running.

## Environment

The following environment is used consistently throughout all the scripts in this repositories.
To verify that the local environment is compliant and able to build the package
a script called 'buildenv_check.sh' exists in the root directory of this
repository. When building under docker, this script is automatically run.

#### Intended architecture
ARCH="${ARCH:-armhf}"

#### Common directory variables
PREFIX="/usr"
BINDIR="${PREFIX}/bin"
DATAROOTDIR="${PREFIX}/share"
DATADIR="${DATAROOTDIR}"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SBINDIR="${EXEC_PREFIX}/sbin"
SYSCONFDIR="${SYSCONFDIR:-/etc}"
SYSTEM_UPDATE_SCRIPT_DIR="${SYSTEM_UPDATE_SCRIPT_DIR:-${LIBEXECDIR}/jedi_system_update.d}"
SYSTEM_UPDATE_CONF_DIR="${SYSTEM_UPDATE_CONF_DIR:-${SYSCONFDIR}/jedi_system_update}"
SUDO_CONF_DIR="${SYSCONFDIR}/sudoers.d"
EXTERNAL_CLEAN_USER_PARTITION_COMMAND="${EXTERNAL_CLEAN_USER_PARTITION_COMMAND:-${SBINDIR}/clean_user_partition.sh}"

#### Build configuration
SRC_DIR="$(pwd)"
BUILD_DIR_TEMPLATE=".build_${ARCH}"
BUILD_DIR="${BUILD_DIR:-${SRC_DIR}/${BUILD_DIR_TEMPLATE}}"

#### Debian package information
PACKAGE_NAME="${PACKAGE_NAME:-jedi-system-update}"
INSTALL_DIR="${INSTALL_DIR:-${DATADIR}/${PACKAGE_NAME}}"
RELEASE_VERSION="${RELEASE_VERSION:-9999.99.99}"

#### Artifact names
TOOLBOX_IMAGE="${TOOLBOX_IMAGE:-um-update_toolbox.xz.img}"
FIRMWARE_UPDATE_FILE="${FIRMWARE_UPDATE_FILE:-um-update.swu}"
DEB_PACKAGE="${PACKAGE_NAME}_${ARCH}-${RELEASE_VERSION}.deb"

## Repository structure
#### Repo root directory:
- License and readme files
- Docker files used by *CI* for generating an isolated build environment.
- Scripts for building the software
- Configuration files for *Gitlab CI*

#### Config directory:
This directory contains configuration files that are used by the
system configuration scripts in the 'scripts' directory. The config files
are installed in the '${SYSTEM_UPDATE_CONF_DIR}'. The configuration files
intended for the toolbox are separated by placing them in the 'toolbox'
subdirectory.
The data save list files are also contained here, ending in the *.keep* suffix.
Files to be excluded during an update end with the *.discard* suffix.
Files listed should be relative paths and be relative from the partition where
they are found.

#### Scripts directory:
Contains update and setup scripts that are installed in '${SYSTEM_UPDATE_SCRIPT_DIR}'. The main entrypoint
'${SBINDIR}/pre_update.sh'. The configuration files intended for the toolbox are separated by placing
them in the 'toolbox' subdirectory. The main entrypoint for the toolbox is '${SBINDIR}/start_update.sh'.
The '${BINDIR}/verify_swupdate.sh' script is used to be verify the correctness
and origin of the software update.

#### Test directory:
Contains a collection of shunit2 based unit tests. The intention is that for
each script in the scripts subdirectory, a matching unit test exists.

The 'run_tests.sh' script runs all tests possible and available. It optionally
takes the toolbox as an argument. If this parameter is supplied, also the legacy
tests are run. Note, that this script does not contain the docker wrappers, use
'build_for_ultimaker.sh' for that.

#### Legacy test directory
Tests written before implementing the shunit2 testing framework. These will
need to be ported to the shunit2 testing framework at some point.
Note that they do depend on subdirectories (fixtures, include) of the test
directory.

##### Fixtures directory:
Testing fixtures needed to run tests, such as for example a software update.

## How to build
This repository is setup to use an existing *Docker* image from this repositories Docker registry.
The image contains all the tools to be able to build the software. When Docker is not installed
the build script will try to build on the host machine and is likely to fail because the required
tools are not installed. When it is necessary to run the build without Docker, execute the
'buildenv_check.sh' script and see if the environment is missing requirements.

By default the build script runs an environment check, builds the image and then validates
it by running tests. The first and the latter can be disabled. Run the help of the
'build_for_ultimaker.sh' script for usage information:
```sh
> ./build_for_ultimaker.sh -h
    Usage: ./build_for_ultimaker.sh [OPTIONS]
        -c   Skip run of build environment checks
        -h   Print usage
        -t   Skip run of rootfs tests
```

## Runtime preparation
A software component responsible for downloading a firmware update will call this repositories '${SBINDIR}/pre_update.sh'
 script.The entrypoint script will then extract or mount the firmware update file and execute the configuration scripts in:
 '${UPDATE_ROOT_MNT}${SYSTEM_UPDATE_SCRIPT_DIR}' in sorted order. The scripts are prefixed with a 2 digit number so
 that we can control what is executed in what order. When all scripts are executed the system will reboot so that
 the toolbox scripts can be executed.

### Preparing the kernel
One necessary step to perform before the reboot to toolbox, is making sure that the printer has the latest Linux Kernel
with the *initrd* environment available. This is not available in pre '5.x' versions and is required to be able to run
the toolbox.

### Preparing the files
The next necessary step is to find a suitable partition for placing the files so that the toolbox scripts can find them.
The needed files include; the 'toolbox image' and the 'update image'. If there is not enough space found on a partition
space needs to be freed on the 'user' partition. The algorithm will first try to perform '${EXTERNAL_CLEAN_USER_PARTITION_COMMAND}',
if that fails or there is still no space available, the algorithm will free space by removing oldest file first until enough space
is available.

## Toolbox
The toolbox is a *squashfs* minimal root filesystem that can be mounted in early boot stages, with the purpose
of providing tools to be able to perform complex setup and update operations. This toolbox image is based on *Linux Alpine*,
 a security-oriented, lightweight Linux distribution based on *Musl Libc* and *Busybox*.
On top of the standard *distro* we install the required tooling, such as; *e2fsprogs-extra f2fs-tools rsync*, resulting in
an image with a compressed size of less then 2.6M at time of writing.

## Adding update or setup routines to the toolbox
During the Kernels initramfs stage this toolbox image is mounted and the entrypoint script '${SBINDIR}/start_update.sh'
will be executed if available.

The system configuration scripts are installed in '${SYSTEM_UPDATE_SCRIPT_DIR}' directory and are prefixed
with a two digit number so the 'start_update.sh' script can sort them.

The um-update_toolbox will serve as a *chroot* environment for the update. Generic execution of all
scripts is required and therefore the scripts are prefixed with a number so they can be sorted and
additional scripts can be added later. As a consequence a execution environment for
the scripts is prepared so that the script can be executed generically.

#### Toolbox runtime environment variables

* TARGET_STORAGE_DEVICE    - The device to perform the system configuration on
* PARTITION_TABLE_FILE     - The partition table file used in the prepare_disk script,
                             default is 'jedi_emmc_sfdisk.table', implicitly the
                             script will look for a counterpart checksum file:
                             '<filename>.sha512'
* UPDATE_ROOT_MNT          - This is the directory within the toolbox root that contains the update files, i.e
                             the extracted or mounted root file system files.
* SYSTEM_UPDATE_CONF_DIR   - The system update directory is a directory within the toolbox root
                             that contains the system update configuration. The configuration files
                             can be found in '/etc/jedi_system_update', i.e. the partition table and data
                             save list.
* SYSTEM_UPDATE_SCRIPT_DIR - The system update scripts directory is a directory within the toolbox root
                             that contains the system update scripts.

# Updating testing fixtures
In some cases, it may be needed to update the testing root file system or the like
may need to be updated.

In all cases:
- Create a new root file system directory (this can be done for example by extracting the current one using tar -x or unsquashfs).
- Create new file/archive
- Update/add tests
- git add/commit

## Updating the test root filesystem
```sh
tar -cJf test/fixtures/test_rootfs.tar.xz -C <path to rootfs> .
```

## Updating the test swu image
Note, that this is from, and has to match with, img_append_signature() from jedi-build/build.sh.

**Note, minimal required GPG version is 2.0.**

```sh
mksquashfs <path to rootfs> test/fixtures/test_um-update.squashfs
gpg --batch --quick-gen-key --yes "ultimaker_development_CI_testing_key"
gpg --export <new key id> > test/fixtures/test_um-gpg.public_key
gpg --batch --detach-sign --default-key <new key id> -output test/fixtures/test_um-update.squashfs.sig --yes test/fixtures/test_um-update.squashfs
cp test/fixtures/test_um-update.squashfs test/fixtures/test_um-update.swu
cat test/fixtures/test_um-update.squashfs.sig >> test/fixtures/test_um-update.swu
printf "%08x" "$(stat -c "%s" "test/fixtures/test_um-update.squashfs.sig")" | tac -rs .. | xxd -r -p >> test/fixtures/test_um-update.swu
```

# Provided interfaces

*) ${BINDIR}/verify_swupdate.sh <update file location> - Verify the firmware image providing the update file location.
*) ${SBINDIR}/pre_update.sh <update file location> - Execute the firmware update, the following parameters are optional and can be
   passed along via environment variables:
   - [UPDATE_IMAGE], the firmware update image file name
   - [TARGET_STORAGE_DEVICE], the target storage device
   Note that if UPDATE_IMAGE/<update file location> if omitted the default is '/tmp/um-update.swu'.

*) ${EXTERNAL_CLEAN_USER_PARTITION_COMMAND} <user data partition> <required bytes> - Cleanup script that is tried to free space.
*) ${SBINDIR}/start_update.sh <toolbox mount> <update mount> <target storage device> - Toolbox entry point interface.

## Interface privileges (sudo/root)
Certain scripts, especially those in the ${SBINDIR} directories require elevated
privileges. A sudoers file is installed with this package to give access to the
group *system-update* to these scripts.

Enabled scripts with sudo are:
- sudo ${SBINDIR}/pre_update.sh <update file location>

# Internal interfaces
## Pre-Update
While still running the full system, we have 3 environment variables that we
pass to all sub-scripts called by the 'pre_update.sh' script. If they are
needed by a sub-script, they are confider ed mandatory. The 'pre_update.sh'
script shall pass these along to all sub-scripts.
- [TARGET_STORAGE_DEVICE], The target device onto which updates will be installed.
- [UPDATE_IMAGE], The update image file to be installed onto '${TARGET_STORAGE_DEVICE}'
- [UPDATE_ROOT_MNT], The location where '${UPDATE_IMAGE}' is mounted

# Updating the docker image in the docker registry
We only want to update the docker image when required and we only want to do this after quality
checks and when merged to master. Therefore Docker container changes should be tested locally first.
To build a container locally all is needed to pass the name of the request image to the build script,
and it will try to pull the image (which will fail) and build and run with the local image instead.
```sh
> CI_REGISTRY_IMAGE="local_test_image_name" ./build_for_ultimaker.sh
```
To make the changes available in the Docker repository follow the instruction as described on the
confluence [CI/CD](https://confluence.ultimaker.com:8443/pages/viewpage.action?pageId=12431561) page.
