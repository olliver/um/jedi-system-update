#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

# common directory variables
PREFIX="/usr"
DATAROOTDIR="${PREFIX}/share"
DATADIR="${DATAROOTDIR}"
EXEC_PREFIX="${PREFIX}"
SBINDIR="${EXEC_PREFIX}/sbin"

CWD="$(dirname "${0}")"


DUMMY_TARGET_STORAGE_DEVICE="dummy_storage_device.img"
FIRMWARE_UPDATE_FILE="um-update.swu"
FIXTURES="${CWD}/fixtures"
START_UPDATE_COMMAND="start_update.sh"
SWUPDATE_TEST_DIR_TEMPLATE="test_swupdate_dir"
SWUPDATE_TEST_IMAGE="test_um-update.swu"
TEST_TOOLBOX_DIR_TEMPLATE="test_toolbox_dir"
TOOLBOX_IMAGE="${TOOLBOX_IMAGE:-${DATADIR}/jedi-system-update/um-update_toolbox.xz.img}"
UPDATE_FILE_SRC_DIR_TEMPLATE="update_file_src_dir"
UPDATE_ROOT_MNT_DIR_TEMPLATE="update_root_mount_dir"

set +eu


oneTimeSetUp()
{
    echo "Initialize toolbox start_update test"

    # Extract the current testing swu image to a temporary directory
    TEST_SWUPDATE_DIR="$(mktemp -d -p "${SHUNIT_TMPDIR}" -t "${SWUPDATE_TEST_DIR_TEMPLATE}.XXXXXXXX")"
    unsquashfs -d "${TEST_SWUPDATE_DIR}/" -f "${FIXTURES}/${SWUPDATE_TEST_IMAGE}"

    # Copy the start_update script from the local repository, into the
    # temporary extracted swu image dir
    mkdir -p "${TEST_SWUPDATE_DIR}/${SBINDIR}"
    cp "${CWD}/../scripts/${START_UPDATE_COMMAND}" "${TEST_SWUPDATE_DIR}/${SBINDIR}/${START_UPDATE_COMMAND}"

    # Create a testing toolbox, with only a dummy start_update.sh script
    TEST_TOOLBOX_DIR="$(mktemp -d -p "${SHUNIT_TMPDIR}" -t "${TEST_TOOLBOX_DIR_TEMPLATE}.XXXXXXXX")"
    mkdir -p "${TEST_TOOLBOX_DIR}/${SBINDIR}"
    cat > "${TEST_TOOLBOX_DIR}/${SBINDIR}/${START_UPDATE_COMMAND}" <<\
________________________________________________________________
#!/bin/sh

if [ "\${#}" -ne 3 ]; then
    exit 1
fi

TOOLBOX_MNT="\${1}"
UPDATE_IMAGE="\${2}"
TARGET_STORAGE_DEVICE="\${3}"

if [ ! -b "\${TARGET_STORAGE_DEVICE}" ] || \
   [ ! -d "\${TOOLBOX_MNT}" ] || \
   [ ! -f "\${UPDATE_IMAGE}" ]; then
    exit 1
fi

exit 0
________________________________________________________________
    chmod +x "${TEST_TOOLBOX_DIR}/${SBINDIR}/${START_UPDATE_COMMAND}"

    # Make a toolbox image out of the testing toolbox and place it in the
    # temporary extracted  swu image dir
    mkdir -p "${TEST_SWUPDATE_DIR}/$(dirname "${TOOLBOX_IMAGE}")/"
    mksquashfs "${TEST_TOOLBOX_DIR}" "${TEST_SWUPDATE_DIR}/${TOOLBOX_IMAGE}" -comp xz

    rm -rf "${TEST_TOOLBOX_DIR:?}/"

    # Make a swu image from the now modified swu image directory
    UPDATE_FILE_SRC_DIR="$(mktemp -d -p "${SHUNIT_TMPDIR}" -t "${UPDATE_FILE_SRC_DIR_TEMPLATE}.XXXXXXXX")"
    mksquashfs "${TEST_SWUPDATE_DIR}" "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" -comp xz

    rm -rf "${TEST_SWUPDATE_DIR:?}/"

    # Extract the start update script just as the init.sh script does
    UPDATE_ROOT_MNT="$(mktemp -d -p "${SHUNIT_TMPDIR}" -t "${UPDATE_ROOT_MNT_DIR_TEMPLATE}.XXXXXXXX")"
    mount "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" "${UPDATE_ROOT_MNT}"
    cp "${UPDATE_ROOT_MNT}/${SBINDIR}/${START_UPDATE_COMMAND}" "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}"
    umount "${UPDATE_ROOT_MNT}"

    echo ""
    echo "Starting start_update tests"
    echo "================================================================================"
}

oneTimeTearDown()
{
    echo "Tearing down toolbox start update test"

    if mountpoint -q "${UPDATE_ROOT_MNT}"; then
        umount "${UPDATE_ROOT_MNT}"
    fi

    if [ -d "${UPDATE_ROOT_MNT}" ]; then
        rmdir "${UPDATE_ROOT_MNT}"
    fi

    if [ -f "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" ]; then
        unlink "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}"
    fi

    if [ -d "${UPDATE_FILE_SRC_DIR}" ] && [ -z "${UPDATE_FILE_SRC_DIR##*${UPDATE_FILE_SRC_DIR_TEMPLATE}*}" ]; then
        rm -rf "${UPDATE_FILE_SRC_DIR:?}/"
    fi

    if [ -d "${TEST_TOOLBOX_DIR}" ] && [ -z "${TEST_TOOLBOX_DIR##*${TEST_TOOLBOX_DIR_TEMPLATE}*}" ]; then
        rm -rf "${TEST_TOOLBOX_DIR:?}/"
    fi

    if [ -d "${TEST_SWUPDATE_DIR}" ]; then
        rm -rf "${TEST_SWUPDATE_DIR:?}/"
    fi
}

setUp()
{
    echo ""

    fallocate -l "4GB" "${SHUNIT_TMPDIR}/${DUMMY_TARGET_STORAGE_DEVICE}"
    TARGET_STORAGE_DEVICE="$(losetup --show --find "${SHUNIT_TMPDIR}/${DUMMY_TARGET_STORAGE_DEVICE}")"
}

tearDown()
{
    if [ -b "${TARGET_STORAGE_DEVICE}" ]; then
        losetup -d "${TARGET_STORAGE_DEVICE}"
        unset TARGET_STORAGE_DEVICE
    fi
    if [ -f "${SHUNIT_TMPDIR}/${DUMMY_TARGET_STORAGE_DEVICE}" ]; then
        unlink "${SHUNIT_TMPDIR}/${DUMMY_TARGET_STORAGE_DEVICE}"
    fi

    echo "--------------------------------------------------------------------------------"
}

testInvalidNumberOfParameters()
{
    echo "Passing only 1 arguments"
    "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}" "one"
    assertFalse "Only 1 arguments should not be allowed." "[ ${?} -eq 0 ]"

    echo "Passing 3 arguments"
    "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}" "1" "2" "3"
    assertFalse "More than 3 arguments should not be allowed." "[ ${?} -eq 0 ]"
}

testWithInvalidStorageDevice()
{
    echo "Passing non-block device as storage device."

    "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}" "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" "/dev/null"
    assertFalse "'/dev/null' should not be accepted as TARGET_STORAGE_DEVICE." "[ ${?} -eq 0 ]"
}

testWithoutUpdateImage()
{
    echo "Passing non-existent UPDATE_IMAGE."

    "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}" "/dev/zero" "${TARGET_STORAGE_DEVICE}"
    assertFalse "An empty update directory should not be accepted." "[ ${?} -eq 0 ]"
}

testToolboxStartOk()
{
    "${UPDATE_FILE_SRC_DIR}/${START_UPDATE_COMMAND}" "${UPDATE_FILE_SRC_DIR}/${FIRMWARE_UPDATE_FILE}" "${TARGET_STORAGE_DEVICE}"
    assertTrue "Failed to run script with proper parameters." "[ ${?} -eq 0 ]"
}

# Load unittest framework
# shellcheck disable=SC1090
. "$(command -v shunit2)"
