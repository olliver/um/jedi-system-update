#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

CWD="$(dirname "${0}")"

BYTES_PER_SECTOR="512"
DEV_MAJOR_MINOR="0:6"
PARTITION_TABLE_FILE="${CWD}/../config/toolbox/jedi_emmc_sfdisk.table"
PREPARE_UPDATE_FILES_COMMAND="${CWD}/../scripts/20_prepare_update_files.sh"
STORAGE_DEVICE_IMG="storage_device.img"
STORAGE_DEVICE_SIZE="7553024" # sectors, about 3.6 GiB
TEST_CONFIG_FILE_DIR="${CWD}/../test/config"

EXTERNAL_CLEANUP_SCRIPT_DIR_TEMPLATE="external_cleanup_scripts"
UPDATE_FILE_SRC_DIR_TEMPLATE="update_file_src_dir"
UPDATE_IMAGE_FILE_NAME="um-update.swu"

is_dev_setup_mounted=false

set +eu


fill_partition()
{
    partition="${1}"

    tmp_mnt="./tmp_mnt"
    mkdir "${tmp_mnt}"
    mount "${partition}" "${tmp_mnt}"

    nrFilesWritten="0"
    while true; do
        fallocate -l 25M "${tmp_mnt}/sparse_file_${nrFilesWritten}" 2> /dev/null || break 1
        nrFilesWritten="$((nrFilesWritten + 1))"
    done

    umount "${tmp_mnt}"
    if [ -d "${tmp_mnt}" ] && [ -z "${tmp_mnt##*tmp_mnt*}" ]; then
        rmdir "${tmp_mnt}"
    fi
}

file_copy_validation()
{
    partition="${1}"

    tmp_mnt="./tmp_mnt"
    mkdir "${tmp_mnt}"
    mount "${partition}" "${tmp_mnt}"

    assertTrue "The update file '${UPDATE_IMAGE_FILE_NAME}' should have been copied to the user partition." \
        "[ -f ${tmp_mnt}/${UPDATE_IMAGE_FILE_NAME} ]"

    umount "${tmp_mnt}"
    if [ -d "${tmp_mnt}" ] && [ -z "${tmp_mnt##*tmp_mnt*}" ]; then
        rmdir "${tmp_mnt}"
    fi
}

check_dist_integrity()
{
    sfdisk -qVl --no-reread "${target_storage_device}"
    e2fsck -fn "${target_storage_device}p1"
    fsck.f2fs "${target_storage_device}p2"
    fsck.f2fs "${target_storage_device}p3"
}

create_dummy_storage_device()
{
    fallocate -l "$((BYTES_PER_SECTOR * STORAGE_DEVICE_SIZE))" "${STORAGE_DEVICE_IMG}"
    sfdisk -qf --no-reread "${STORAGE_DEVICE_IMG}" < "${PARTITION_TABLE_FILE}"

    target_storage_device="$(losetup --show --find --partscan "${STORAGE_DEVICE_IMG}")"

    mkfs.ext4 -F -L "boot" -O ^extents,^64bit "${target_storage_device}p1"
    mkfs.f2fs -l "root" "${target_storage_device}p2"
    mkfs.f2fs -l "user" "${target_storage_device}p3"

    check_dist_integrity
}

oneTimeSetUp()
{
    echo "Initialize prepare update files test"

    echo ""
    echo "Starting prepare update files tests"
    echo "================================================================================"
}

oneTimeTearDown()
{
    echo "Tearing down prepare update files test"
}

setUp()
{
    echo ""

    # devtmps needs to be re-mounted when running in docker
    if [ "$(mountpoint -d -q /dev)" != "${DEV_MAJOR_MINOR}" ]; then
        mount -t "devtmpfs" "none" "/dev"
        is_dev_setup_mounted=true
    fi

    EXTERNAL_CLEANUP_SCRIPT_DIR="$(mktemp -d -t "${EXTERNAL_CLEANUP_SCRIPT_DIR_TEMPLATE}.XXXXXXXX")"
    UPDATE_FILE_SRC_DIR="$(mktemp -d -t "${UPDATE_FILE_SRC_DIR_TEMPLATE}.XXXXXXXX")"

    fallocate -l 35MiB "${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}"

    create_dummy_storage_device 1> /dev/null
}

tearDown()
{
    if [ -b "${target_storage_device}" ]; then
        losetup -d "${target_storage_device}"
        target_storage_device=""
    fi

    if [ -d "${UPDATE_FILE_SRC_DIR}" ] && [ -z "${UPDATE_FILE_SRC_DIR##*${UPDATE_FILE_SRC_DIR_TEMPLATE}*}" ]; then
        rm -rf "${UPDATE_FILE_SRC_DIR:?}/"
    fi

    if "${is_dev_setup_mounted}"; then
        umount "/dev"
        is_dev_setup_mounted=false
    fi

    echo "--------------------------------------------------------------------------------"
}

testInvalidTargetStorageDeviceArgument()
{
    eval "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "${PREPARE_UPDATE_FILES_COMMAND}" 1> /dev/null
    assertFalse "The script should fail when no storage device is set." "[ ${?} -eq 0 ]"

    eval "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "TARGET_STORAGE_DEVICE=/dev/tty1" \
         "${PREPARE_UPDATE_FILES_COMMAND}" 1> /dev/null
    assertFalse "The script should fail when the given storage device not a block device." "[ ${?} -eq 0 ]"
}

testInvalidUpdateFileSrcDirArgument()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "${PREPARE_UPDATE_FILES_COMMAND}" 1> /dev/null
    assertFalse "The script should fail when no target file source dir is set." "[ ${?} -eq 0 ]"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}" \
         "${PREPARE_UPDATE_FILES_COMMAND}" 1> /dev/null
    assertFalse "The script should fail when the the update image file." "[ ${?} -eq 0 ]"
}

testEnoughSpaceOnPartitionToPlaceFiles()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"
    assertTrue "The script should not fail because there is enough space." "[ ${?} -eq 0 ]"

    file_copy_validation "${target_storage_device}p2"
}

testSpaceFreedOnUserPartitionToPlaceFiles()
{
    fill_partition "${target_storage_device}p1"
    fill_partition "${target_storage_device}p2"
    fill_partition "${target_storage_device}p3"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"
    assertTrue "The script failed to make space available on the user partition." "[ ${?} -eq 0 ]"

    file_copy_validation "${target_storage_device}p3"
}

testDoNotFreeSpaceBecauseTheUpdateFilesAreTooBigAnyway()
{
    unlink "${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}"
    fallocate -l "$((BYTES_PER_SECTOR * STORAGE_DEVICE_SIZE))" "${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"

    assertFalse "The script should have failed because the update file's will not fit anywhere." "[ ${?} -eq 0 ]"
}

testTryToExecuteNoneExistingExternalCleanupScriptFallBackToDefault()
{
    fill_partition "${target_storage_device}p2"
    fill_partition "${target_storage_device}p3"

    external_clean_script="${EXTERNAL_CLEANUP_SCRIPT_DIR}/external_clean_script.sh"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "EXTERNAL_CLEAN_USER_PARTITION_COMMAND=${external_clean_script}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"

    assertTrue "The script should have fallen back to default user partition cleanup routine." "[ ${?} -eq 0 ]"

    file_copy_validation "${target_storage_device}p3"
}

testTryToExecuteFailingExternalCleanupScriptFallBackToDefault()
{
    fill_partition "${target_storage_device}p2"
    fill_partition "${target_storage_device}p3"

    external_clean_script="${EXTERNAL_CLEANUP_SCRIPT_DIR}/external_clean_script.sh"
    cat > "${external_clean_script}" <<\
________________________________________________________
#!/bin/sh
echo "Executing cleanup script ${@}"
exit 1
________________________________________________________
    chmod +x "${external_clean_script}"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "EXTERNAL_CLEAN_USER_PARTITION_COMMAND=${external_clean_script}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"

    assertTrue "The script should not have failed, it should have fallen back to default user partition cleanup routine." "[ ${?} -eq 0 ]"

    file_copy_validation "${target_storage_device}p3"
}

testExecuteExampleExternalCleanupScriptDefaultCleanupNotExecuted()
{
    fill_partition "${target_storage_device}p2"

    tmp_mnt="./tmp_mnt"
    mkdir "${tmp_mnt}"
    mount "${target_storage_device}p3" "${tmp_mnt}"

    nr_files_written="0"
    while [ "${nr_files_written}" -lt "3" ]; do
        fallocate -l 50M "${tmp_mnt}/other_sparse_file_${nr_files_written}" 2> /dev/null
        nr_files_written="$((nr_files_written + 1))"
    done

    umount "${tmp_mnt}"
    if [ -d "${tmp_mnt}" ] && [ -z "${tmp_mnt##*tmp_mnt*}" ]; then
        rmdir "${tmp_mnt}"
    fi

    fill_partition "${target_storage_device}p3"

    external_clean_script="${EXTERNAL_CLEANUP_SCRIPT_DIR}/external_clean_script.sh"
    cp "${TEST_CONFIG_FILE_DIR}/external_clean_script.sh" "${external_clean_script}"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" \
         "UPDATE_IMAGE=${UPDATE_FILE_SRC_DIR}/${UPDATE_IMAGE_FILE_NAME}" \
         "EXTERNAL_CLEAN_USER_PARTITION_COMMAND=${external_clean_script}" \
         "${PREPARE_UPDATE_FILES_COMMAND}"
    assertTrue "The script should not have failed." "[ ${?} -eq 0 ]"

    file_copy_validation "${target_storage_device}p3"

    tmp_mnt="./tmp_mnt"
    mkdir "${tmp_mnt}"
    mount "${target_storage_device}p3" "${tmp_mnt}"

    nr_normal_sparse_files="$(find "${tmp_mnt}" -type f -iname "sparse_file_*" | wc -l)"
    nr_other_sparse_files="$(find "${tmp_mnt}" -type f -iname "other_sparse_file_*" | wc -l)"

    umount "${tmp_mnt}"
    if [ -d "${tmp_mnt}" ] && [ -z "${tmp_mnt##*tmp_mnt*}" ]; then
        rmdir "${tmp_mnt}"
    fi

    assertTrue "All the 'other_sparse' files should have been removed by the cleanup script" \
        "[ ${nr_other_sparse_files} -eq 0 ]"

    assertTrue "Only the 'other_sparse' files should have been removed by the cleanup script" \
        "[ ${nr_normal_sparse_files} -gt 0 ]"
}

# Load unittest framework
# shellcheck disable=SC1090
. "$(command -v shunit2)"
