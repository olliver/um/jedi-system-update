#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

CWD="$(dirname "${0}")"

SIGNING_KEY_ID="F38D553A15793FCCC70305A616EB7C7E3FE06BCA"
SIG_SIZE_BYTES="4"
SWUPDATE_FILE_NAME="test_swupdate.swu"
FIXTURES="${CWD}/../test/fixtures"
VERIFY_SWUPDATE_COMMAND="${CWD}/../scripts/verify_swupdate.sh"

set +eu


random_int_range()
{
    shuf -n 1 -i "${1}"-"${2}"
}

random_int()
{
    random_int_range "0" "${1}"
}

extract_meta_sizes()
{
    UPDATE_FILE="${1}"

    FILE_SIZE="$(stat -c "%s" "${UPDATE_FILE}")"
    SIG_SIZE="$(printf "%u" "0x$(tail -c "${SIG_SIZE_BYTES}" "${UPDATE_FILE}" | od -An -t x4 | tr -d " ")")"
    PAYLOAD_SIZE="$((FILE_SIZE - SIG_SIZE - SIG_SIZE_BYTES))"
}

oneTimeSetUp()
{
    echo "Initialize verify swupdate setup"

    if ! gpg --import "${FIXTURES}/test_um-gpg.public_key"; then
        echo "Failed to import GPG key, cannot continue."
        exit 1
    fi

    echo ""
    echo "Starting verify swupdate tests"
    echo "================================================================================"
}

oneTimeTearDown()
{
    echo "Tearing down verify swupdate test"

    if ! gpg --batch --delete-secret-and-public-key --yes "${SIGNING_KEY_ID}"; then
        echo "Failed to remove GPG key '${SIGNING_KEY_ID}'."
    fi
}

setUp()
{
    echo ""

    SWUPDATE_FILE="${SHUNIT_TMPDIR}/${SWUPDATE_FILE_NAME}"
    cp "${FIXTURES}/test_um-update.swu" "${SWUPDATE_FILE}"
    extract_meta_sizes "${SWUPDATE_FILE}"
}

tearDown()
{
    unlink "${SWUPDATE_FILE}"

    echo "--------------------------------------------------------------------------------"
}

testSWUpdateZeroSignatureSize()
{
    echo "Zeroing update file signature size bytes."
    dd \
        bs=1 \
        conv=notrunc \
        count=${SIG_SIZE_BYTES} \
        if="/dev/zero" \
        of="${SWUPDATE_FILE}" \
        seek="$((PAYLOAD_SIZE + SIG_SIZE))" \
        > /dev/null

    "${VERIFY_SWUPDATE_COMMAND}" "${SWUPDATE_FILE}"
    assertFalse "Zero size signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateRandomSignatureSize()
{
    echo "Scrambling update file signature size bytes."
    # Reduce the chance we can get the same random bytes
    retry_count=10
    while [ "${retry_count}" -ne 0 ]; do
        random_sig_bytes="$(head -c "${SIG_SIZE_BYTES}" "/dev/urandom")"
        random_sig_size="$(printf "%u" "0x$(printf "%b" "${random_sig_bytes}" | od -An -t x4 | tr -d " ")")"

        if [ "${SIG_SIZE}" -ne "${random_sig_size}" ]; then
            break
        fi

        retry_count="$((retry_count - 1))"
    done
    if [ "${retry_count}" -eq 0 ]; then
        fail "Unable to obtain random signature bytes."
    fi

    printf "%b" "${random_sig_bytes}" | dd \
        bs=1 \
        conv=notrunc \
        count="${SIG_SIZE_BYTES}" \
        of="${SWUPDATE_FILE}" \
        seek="$((PAYLOAD_SIZE + SIG_SIZE))" \
        > /dev/null

    "${VERIFY_SWUPDATE_COMMAND}" "${SWUPDATE_FILE}"
    assertFalse "Invalid signature size should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateBadSignature()
{
    echo "Corrupting update file signature."

    SIG_OVERWRITE_SIZE="8"
    RANDOM_SIG_POS="$(random_int "$((PAYLOAD_SIZE - SIG_OVERWRITE_SIZE))")"

    dd \
        bs=1 \
        conv=notrunc \
        count="${SIG_OVERWRITE_SIZE}" \
        if="/dev/urandom" \
        of="${SWUPDATE_FILE}" \
        seek="${RANDOM_SIG_POS}" \
        > /dev/null

    "${VERIFY_SWUPDATE_COMMAND}" "${SWUPDATE_FILE}"
    assertFalse "Bad signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateCorruptPayload()
{
    echo "Corrupting update file payload"

    PAYLOAD_OVERWRITE_SIZE="256"
    RANDOM_PAYLOAD_POS="$(random_int "$((PAYLOAD_SIZE - PAYLOAD_OVERWRITE_SIZE))")"

    dd \
        bs=1 \
        conv=notrunc \
        count="${PAYLOAD_OVERWRITE_SIZE}" \
        if="/dev/urandom" \
        of="${SWUPDATE_FILE}" \
        seek="${RANDOM_PAYLOAD_POS}" \
        > /dev/null

    "${VERIFY_SWUPDATE_COMMAND}" "${SWUPDATE_FILE}"
    assertFalse "Corrupted payload should not be accepted." "[ ${?} -eq 0 ]"
}

testValidSWUpdateFile()
{
    "${VERIFY_SWUPDATE_COMMAND}" "${SWUPDATE_FILE}"
    assertTrue "Correct update should be accepted." "[ ${?} -eq 0 ]"
}

# Load unittest framework
# shellcheck disable=SC1090
. "$(command -v shunit2)"
