#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

CWD="$(dirname "${0}")"

BYTES_PER_SECTOR="512"
DEV_MAJOR_MINOR="0:6"

STORAGE_DEVICE_IMG="storage_device.img"
STORAGE_DEVICE_SIZE="7553024" # sectors, about 3.6 GiB
FIXTURES="${CWD}/../test/fixtures/boot_files"
UPDATE_ROOT_MNT_TEMPLATE="temp_update_root_mnt"

PARTITION_TABLE_FILE="${CWD}/../config/toolbox/jedi_emmc_sfdisk.table"
UPDATE_BOOT_FILES_COMMAND="${CWD}/../scripts/toolbox/40_update_boot_files.sh"

set +eu


check_dist_integrity()
{
    sfdisk -qVl --no-reread "${target_storage_device}"
    e2fsck -fn "${target_storage_device}p1"
    fsck.f2fs "${target_storage_device}p2"
    fsck.f2fs "${target_storage_device}p3"
}

create_dummy_storage()
{
    dd if="/dev/zero" of="${STORAGE_DEVICE_IMG}" bs="1" count="0" \
        seek="$((BYTES_PER_SECTOR * STORAGE_DEVICE_SIZE))" status=none

    sfdisk -qf --no-reread "${STORAGE_DEVICE_IMG}" < "${PARTITION_TABLE_FILE}"

    target_storage_device="$(losetup --show --find --partscan "${STORAGE_DEVICE_IMG}")"

    mkfs.ext4 -F -L "boot" -O ^extents,^64bit "${target_storage_device}p1"
    mkfs.f2fs -l "root" "${target_storage_device}p2"
    mkfs.f2fs -l "user" "${target_storage_device}p3"

    check_dist_integrity
}

oneTimeSetUp()
{
    echo "Initialize toolbox start_update test"

    echo ""
    echo "Starting start_update tests"
    echo "================================================================================"
}

oneTimeTearDown()
{
    echo "Tearing down toolbox start update test"
}

setUp()
{
    echo ""

    # devtmps needs to be re-mounted when running in docker
    if [ "$(mountpoint -d -q /dev)" != "${DEV_MAJOR_MINOR}" ]; then
        mount -t "devtmpfs" "none" "/dev"
        is_dev_setup_mounted=true
    fi

    update_root_mnt="$(mktemp -d -t "${UPDATE_ROOT_MNT_TEMPLATE}.XXXXXXX")"

    mkdir -p "${update_root_mnt}/boot"
    tar -xf "${FIXTURES}/all_boot_partition_files.tar" -C "${update_root_mnt}/boot/"

    create_dummy_storage 1> /dev/null
}

tearDown()
{
    if [ -b "${target_storage_device}" ]; then
        losetup -d "${target_storage_device}"
        target_storage_device=""
    fi

    if [ -f "${STORAGE_DEVICE_IMG}" ]; then
        unlink "${STORAGE_DEVICE_IMG}"
    fi

    if [ -d "${update_root_mnt}" ] && [ -z "${update_root_mnt##*${UPDATE_ROOT_MNT_TEMPLATE}*}" ]; then
        rm -r "${update_root_mnt}"
    fi

    if "${is_dev_setup_mounted}"; then
        umount "/dev"
        is_dev_setup_mounted=false
    fi

    echo "--------------------------------------------------------------------------------"
}

testUpdateRootDirNotGivenScriptFails()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" "${UPDATE_BOOT_FILES_COMMAND}"
    assertFalse "Script should have failed because 'UPDATE_ROOT_MNT' is not given." "[ ${?} -eq 0 ]"

    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" "UPDATE_ROOT_MNT=/some_none_existent_dir" "${UPDATE_BOOT_FILES_COMMAND}"
    assertFalse "Script should have failed because given 'UPDATE_ROOT_MNT' does not exist." "[ ${?} -eq 0 ]"
}

testUpdateBootFiles()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" "UPDATE_ROOT_MNT=${update_root_mnt}" "${UPDATE_BOOT_FILES_COMMAND}"
    assertTrue "Script should have executed without error." "[ ${?} -eq 0 ]"

    boot_partition_mnt="$(mktemp -d -t "boot_partition_mnt.XXXXXXX")"
    mount "${target_storage_device}p1" "${boot_partition_mnt}"

    tar -dvf "${FIXTURES}/emmc_boot_partition_files.tar" -C "${boot_partition_mnt}"
    assertTrue "The boot partition files should match the test archive contents." "[ ${?} -eq 0 ]"

    umount "${boot_partition_mnt}"
    if [ -z "${boot_partition_mnt##*boot_partition_mnt*}" ]; then
        rmdir "${boot_partition_mnt}"
    fi
}

testUbootCorrectlyProgrammed()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" "UPDATE_ROOT_MNT=${update_root_mnt}" "${UPDATE_BOOT_FILES_COMMAND}"
    assertTrue "Script should have executed without error." "[ ${?} -eq 0 ]"

    programmed_uboot_file="programmed_uboot_file"
    new_uboot_file="${FIXTURES}/u-boot-sunxi-with-spl.bin"
    uboot_start=8192
    byte_size="$(stat -c "%s" "${new_uboot_file}")"

    dd if="${target_storage_device}" of="${programmed_uboot_file}" bs=1 skip="${uboot_start}" \
        count="${byte_size}" 1> /dev/null

    cmp "${new_uboot_file}" "${programmed_uboot_file}"

    assertTrue "Unexpected difference between U-Boot update file and programmed file." "[ ${?} -eq 0 ]"

    unlink "${programmed_uboot_file}"
}

testUbootEnvCorrectlyProgrammed()
{
    eval "TARGET_STORAGE_DEVICE=${target_storage_device}" "UPDATE_ROOT_MNT=${update_root_mnt}" "${UPDATE_BOOT_FILES_COMMAND}"
    assertTrue "Script should have executed without error." "[ ${?} -eq 0 ]"

    programmed_uboot_env_file="programmed_uboot_env_file"
    new_uboot_env_file="${FIXTURES}/um_u-boot.env.bin"
    uboot_env_start=557056
    byte_size="$(stat -c "%s" "${new_uboot_env_file}")"

    dd if="${target_storage_device}" of="${programmed_uboot_env_file}" bs=1 skip="${uboot_env_start}" \
        count="${byte_size}" 1> /dev/null

    cmp "${new_uboot_env_file}" "${programmed_uboot_env_file}"

    assertTrue "Unexpected difference between U-Boot environment update file and programmed file ." "[ ${?} -eq 0 ]"

    unlink "${programmed_uboot_env_file}"
}

# Load unittest framework
# shellcheck disable=SC1090
. "$(command -v shunit2)"
