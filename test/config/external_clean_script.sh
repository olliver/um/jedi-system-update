#!/bin/sh

set -eu

echo "Executing cleanup script ${*}"

tmp_mnt="./tmp_mnt"
mkdir "${tmp_mnt}"
mount "${1}" "${tmp_mnt}"

for file in "${tmp_mnt}/other_sparse_file_"*; do
    unlink "${file}"
done

umount "${tmp_mnt}"
if [ -d "${tmp_mnt}" ] && [ -z "${tmp_mnt##*tmp_mnt*}" ]; then
    rmdir "${tmp_mnt}"
fi

exit 0
