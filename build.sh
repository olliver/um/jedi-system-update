#!/bin/sh
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

set -eu

ARCH="${ARCH:-armhf}"
ARM_EMU_BIN="${ARM_EMU_BIN:-}"

# common directory variables
PREFIX="/usr"
BINDIR="${PREFIX}/bin"
DATAROOTDIR="${PREFIX}/share"
DATADIR="${DATAROOTDIR}"
EXEC_PREFIX="${PREFIX}"
LIBEXECDIR="${EXEC_PREFIX}/libexec"
SBINDIR="${EXEC_PREFIX}/sbin"
SYSCONFDIR="${SYSCONFDIR:-/etc}"

SPLASH_SCREEN_UPDATE="um-update_splash.bmp"
SYSTEM_UPDATE_SCRIPT_DIR="${LIBEXECDIR}/jedi_system_update.d"
SUDO_CONF_DIR="${SYSCONFDIR}/sudoers.d"
MODULES_LOAD_DIR="${SYSCONFDIR}/modules-load.d"

SRC_DIR="$(pwd)"
BUILD_DIR_TEMPLATE=".build_${ARCH}"
BUILD_DIR="${BUILD_DIR:-${SRC_DIR}/${BUILD_DIR_TEMPLATE}}"

# Debian package information
PACKAGE_NAME="${PACKAGE_NAME:-jedi-system-update}"
INSTALL_DIR="${INSTALL_DIR:-${DATADIR}/${PACKAGE_NAME}}"
RELEASE_VERSION="${RELEASE_VERSION:-9999.99.99}"

TOOLBOX_IMAGE="${TOOLBOX_IMAGE:-um-update_toolbox.xz.img}"


build_toolbox()
{
    if [ -x "./build_toolbox.sh" ]; then
        "./build_toolbox.sh"
    fi
}

create_debian_package()
{
    DEB_DIR="${BUILD_DIR}/debian_deb_build"

    mkdir -p "${DEB_DIR}/DEBIAN"
    sed -e 's|@ARCH@|'"${ARCH}"'|g' \
        -e 's|@PACKAGE_NAME@|'"${PACKAGE_NAME}"'|g' \
        -e 's|@RELEASE_VERSION@|'"${RELEASE_VERSION}"'|g' \
        "${SRC_DIR}/debian/control.in" > "${DEB_DIR}/DEBIAN/control"
    cp "${SRC_DIR}/debian/postinst" "${DEB_DIR}/DEBIAN/postinst"

    mkdir -p "${DEB_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}"
    cp "${SRC_DIR}/scripts/"[0-9][0-9]*".sh" "${DEB_DIR}/${SYSTEM_UPDATE_SCRIPT_DIR}"

    mkdir -p "${DEB_DIR}/${BINDIR}"
    cp "${SRC_DIR}/scripts/verify_swupdate.sh" "${DEB_DIR}/${BINDIR}"

    mkdir -p "${DEB_DIR}/${SBINDIR}"
    cp "${SRC_DIR}/scripts/pre_update.sh" "${DEB_DIR}/${SBINDIR}"
    cp "${SRC_DIR}/scripts/start_update.sh" "${DEB_DIR}/${SBINDIR}"

    mkdir -p "${DEB_DIR}/${SUDO_CONF_DIR}"
    sed -e 's|@SBINDIR@|'"${SBINDIR}"'|g' \
        "${SRC_DIR}/config/30_jedi_system_update_sudoers.in" > \
        "${DEB_DIR}/${SUDO_CONF_DIR}/30_jedi_system_update_sudoers"

    mkdir -p "${DEB_DIR}/${MODULES_LOAD_DIR}"
    cp "${SRC_DIR}/config/modules-load.d/jedi-system-update.conf" "${DEB_DIR}/${MODULES_LOAD_DIR}/"

    mkdir -p "${DEB_DIR}/${INSTALL_DIR}"
    cp "${BUILD_DIR}/${TOOLBOX_IMAGE}" "${DEB_DIR}/${INSTALL_DIR}/"

    # TODO Remove when implementing EMP-483
    mkdir -p "${DEB_DIR}/boot"
    gzip --best -c "splash/${SPLASH_SCREEN_UPDATE}" > "${DEB_DIR}/boot/${SPLASH_SCREEN_UPDATE}.gz"

    dpkg-deb --build "${DEB_DIR}" "${BUILD_DIR}/${PACKAGE_NAME}_${ARCH}-${RELEASE_VERSION}.deb"

    dpkg-deb -c "${BUILD_DIR}/${PACKAGE_NAME}_${ARCH}-${RELEASE_VERSION}.deb"
}

cleanup()
{
    if [ -z "${BUILD_DIR##*${BUILD_DIR_TEMPLATE}*}" ]; then
        rm -rf "${BUILD_DIR:?}"
    fi
}

usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "  -c   Explicitly cleanup the build directory"
    echo "  -h   Print this usage"
    echo "NOTE: This script requires root permissions to run."
}

while getopts ":hc" options; do
    case "${options}" in
    c)
        cleanup
        exit 0
        ;;
    h)
        usage
        exit 0
        ;;
    :)
        echo "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo "Invalid option: -${OPTARG}"
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"

if [ "$(id -u)" -ne 0 ]; then
    echo "Warning: this script requires root permissions."
    echo "Run this script again with 'sudo ${0}'."
    echo "See ${0} -h for more info."
    exit 1
fi

cleanup
build_toolbox
create_debian_package

exit 0
