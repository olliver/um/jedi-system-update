#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: LGPL-3.0+

FROM registry.hub.docker.com/library/debian:stretch

LABEL Maintainer="software-embedded-platform@ultimaker.com" \
      Comment="Ultimaker update-tools filesystem"

RUN apt-get update && apt-get install -y \
        coreutils \
        dpkg \
        e2fsprogs \
        f2fs-tools \
        gpg \
        rsync \
        shunit2 \
        squashfs-tools \
        tar \
        util-linux \
        wget \
        xz-utils \
    && \
    apt-get clean && \
    rm -rf /var/cache/apt/* && \
    rm -rf /var/lib/apt/* && \
    echo "HACK: Work around gpg and gpgv file finding. See EMP-475." && \
    mkdir -p ~/.gnupg && \
    ln -fs pubring.gpg ~/.gnupg/trustedkeys.gpg && \
    ln -fs pubring.kbx ~/.gnupg/trustedkeys.kbx && \
    chmod og-rx -R ~/.gnupg

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
